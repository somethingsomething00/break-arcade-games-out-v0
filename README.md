# Break Arcade Games Out

This is a fork of the original [Break Arcade Games Out](https://github.com/DanZaidan/break_arcade_games_out) by [Dan Zaidan](https://github.com/DanZaidan)

The game is a breakout-style game themed after popular arcade games.

This project features 3 new ports and several minor bugfixes and additions to the game code.

The source code is largely platform agnostic, with the only caveat being that one needs to implement a platform binding layer to handle input and provide the game with a valid framebuffer so that it can render pixels to it, and that we can display after it has finished its update. Sound is supported by the game but has been disabled for this release (see [Sound](#sound)).

The original binding layer was written for Windows, which is included here as well.

![thumbnail](./thumbnail.png)

![thumbnail](./scr00.png)


## Ports
New ports consist of:
- GL / glfw
- X11
- SDL2


## Dependencies
GCC or Clang on Linux (Clang is the default)

Cl on Windows

The rest will depend on the target.

### GL
- OpenGL (Optional: OpenGL 2.1 or higher required to use Pixel Buffer Objects)
- glfw3

### X11
- X11
- Xrandr
- X11 Shared Memory Extension (optional)

### SDL2
- SDL2


## To build (Linux)
Run the [build.sh](code/build.sh) file.

With no arguments, X11 is the default target.

See `build.sh -h` for all possible build targets.

```console
# Default X11 target
# Also x11-release
./build.sh

# OpenGL target with optimizations
./build.sh gl-release

# SDL target with optimizations
./build.sh sdl-release
```
The script will accept proceeding arguments as compiler flags, provided that the first argument is a valid target.

```console
./build.sh gl-release -DGL_USE_PBO=1
```

## To build (Windows)
The project comes with a [build.bat](code/build.bat) file, which in theory works similarly to the Unix build.sh file in that you only need to run this one file to build the whole project.

By default, the build.bat file compiles with cl.exe and builds [win32_platform.c](code/win32_platform.c). The new additions have not been included in the batch file.

The OpenGl and SDL2 ports *should* work on Windows without much issue if anyone fancies building them, as they contain no platform specific code themselves.

See the original [README](./README_original.md) file for instructions on how to build on Windows.


## New features in this release
- Introduce screen shake as a config option. Before, it was always on.

- Introduce live-reloading of the config file during gameplay.

- Introduce default values if `config.txt` is missing or if config values are completely absent from the file (i.e not a parse error).


## Controls

`Left-Mouse-Button` Select level in main menu.

`Mouse-movement` Move the player paddle across the screen in the x-axis.

`f` Toggle fullscreen.

`Alt-Enter` Toggle fullscreen.

`Esc` Exit the current level and go back to the main menu.

`l` Live-reload the config file.

`p` Pause.

`r` Reset the platform profiler counter.

`q` Quit the game.

`Alt-F4` Quit the game.

### Developer mode
`Down` Continously break all remaining blocks in the level.

`Up` Invincibility.

`Right` Skip the current level and advance to the next one.


## Config (Game)
The game supports a rudimentary config file, `config.txt`.

The following options are accepted:

`mouse_sensitivity` (0.1 to 20.0)

`screen_shake` (true/false)

`windowed` (true/false) (Only implemented in the SDL port for now)

`lock_fps` (true/false) (Note: Does not do anything at the moment in the new ports, the frame rate is always capped at the native refresh rate of the monitor).

You may set the options in the following way:

`key = value`


```ini
mouse_sensitivity = 5.0

windowed = false
```


## Compile time switches

See [config.h](code/config.h). They are also described below.


### Linux / Windows
Pass `-DLATFORM_PROFILER=1` to enable the platform profiler. This enables profiling of code blocks using the target operating system's native high resolution timer. See [platform_profiler.h](code/platform_profiler.h) for more documentation. 

Note that this particular profiler is a new addition, and by this define will not do anything in the Windows version, since the source code to that file has been unchanged from the original. To use it without modifying the batch file, define `PLATFORM_PROFILER=1` in `win32_platform.c` and include `platform_profiler.h` and `platform_profiler.c` below it.


### X11
Pass `-DX_USE_SHM=1` to enable the X11 Shared Memory extension. This dramatically speeds up the copying of the framebuffer to the X server, and provides a smoother game experience. It's on by default, so you'll need disable it if your platform doesn't have this extension.


### GL
Pass `-DGL_USE_PBO=1` to enable the use of pixel buffer objects. This allows for asynchronous copies of the game framebuffer to the graphics card, which may increase performance. Note that you will need a version of OpenGL of `2.1` or greater to use this feature.


### SDL
Pass `-DSDL_NOCHECK=1` to disable runtime checking of function calls made by SDL. It's enabled by default.


### Game
Pass `-DDEVELOPMENT=1` to enable the developer mode. [Controls](#controls).

Pass `-DPROFILER=1` to enable the native in-game profiler. **Note:** If you choose to port this game, you will need to call `begin_profiler` followed by `render_profiler` in your main game loop to actually draw it, the game doesn't call these for you.


## Sound
Sound is currently disabled on Linux, mainly because I do not know how to program sound on Linux yet. This is accomplished by a series of `LINUX_NO_SOUND` defines in the game code where compilation proved to be an issue. Omit the `LINUX_NO_SOUND` define to enable the sound codepaths again, but be aware that the game requires a sound implementation in the platform layer to work properly.


## Goals of the project
- Learn how to write a platform layer for a platform-independent game.
- Learn X11 window creation, event handling, and rendering.
- Learn the basics of SDL2.


## Known bugs
- In all circumstances, having the frametime exceed ~60ms per frame causes some strange bugs in the game engine itself, rendering the game almost unplayable. I have not investigated further as to why this happens.

- The level advance mechanic in developer mode is slightly busted on the Tetris level. Due to how the blocks are spawned and how the victory condition is checked, it requires two presses to advance.

- The sound is disabled in all of the new ports. See [Sound](#sound).


## Porting
If you wish to port this code to a different platform or a different language, here is some pseudocode to get you familiar with the basics. Note that I did not include sound code, nor the file IO that that game does. See [`platform_common.c`](code/platform_common.c) to understand what functions the game expects to have provided.

```c
int main()
{
	int render_buffer_width = 720;
	int render_buffer_height = 1280;
	// Can be anything, but the game is hardcoded to run at a 16:9 aspect ratio so this looks the nicest.
	
	global Render_Buffer render_buffer;
	// This is the global pixel buffer that the game uses. 
	// Note that `render_buffer` is the actual variable name that the game expects internally.
	// Declare this in the global scope of your language
	// The pixel format is BGRA_32
	// Defined in platform_common.c
	
	Input game_input;
	// Input struct that's used internally within the game to represent various mouse and key states.
	// Defined in platform_common.c
	
	
	init_game_buffer(render_buffer, render_buffer_width, render_buffer_height); 
	// Allocate enough memory for it and set its members accordingly.
	
	init_native_buffer(); 
	// This is your native framebuffer. On some platforms you don't need to do this and can use the game framebuffer directly.
	
	while(!time_to_quit)
	{
		handle_input(game_input);
		// Platform layer call. Map your platform's input into what the game can use.
		
		update_game(game_input, delta_time_in_seconds);
		// Call the game code. All of the magic happens here.
		// Note that this is the actual name of the function call in the source code.
		
		draw_game();
		// Platform layer call. Render the resulting pixel buffer to the screen.
		
		vsync();
		// Optional: Some function that caps the frame rate at your monitor's native refresh rate.
	}
}

// Here you implement each function in whatever way is best suited for your OS/language to do each task.
// The game calls these to do basic IO.
os_read_entire_file() {}
os_write_save_file() {}
os_free_file() {}
...
// These are declared in platform_common.c
```

## Final notes
Do check out the archived [livestreams](https://www.youtube.com/playlist?list=PL7Ej6SUky1357r-Lqf_nogZWHssXP-hvH) by Dan. He shows the entire process of creating the original game, from the first line of code to the last, and there is some great educational value to be had from individual episodes even if you don't plan on watching the entire series.

## References
[Break Arcade Games Out (Github - Original source)](https://github.com/DanZaidan/break_arcade_games_out)

[Break Arcade Games Out Livestream (Development process by the original author)](https://www.youtube.com/playlist?list=PL7Ej6SUky1357r-Lqf_nogZWHssXP-hvH)

[Xlib Programming Manual](https://tronche.com/gui/x/xlib/)

[X11 Shared memory extension](https://x.org/releases/X11R7.7/doc/xextproto/shm.html)

[Extended Window Manager Hints](https://specifications.freedesktop.org/wm-spec/wm-spec-1.3.html)

[GLFW3 documentation](https://www.glfw.org/docs/latest/)

[OpenGL documentation](https://docs.gl/)

[SDL Documentation](http://wiki.libsdl.org/) *(They finally have offline docs!)*

[Doomgeneric](https://github.com/ozkl/doomgeneric) A similar idea to this game in some respects, which makes porting Doom to most platforms an absolute breeze.
