#ifndef _LINUX_GL_PLATFORM_H_
#define _LINUX_GL_PLATFORM_H_

// Standard
//--------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


// GL
//--------------------------------------------------------------------------------
#include <GL/glew.h>
#include <GL/gl.h>
#include <GLFW/glfw3.h>


/**********************************
* Platform types
**********************************/
typedef struct
{
	u32 width;
	u32 height;

	s32 windowedWidth;
	s32 windowedHeight;
	s32 windowedX;
	s32 windowedY;
} gl_Screen_t;

typedef struct
{
	u32 *data;
	GLuint id;
	GLuint pbo[2];
	u32 pboIndex;
	u32 width;
	u32 height;
	u32 bpp;
	u32 size;
} gl_Texture;

enum
{
	GL_VSYNC_DISABLE,
	GL_VSYNC_ENABLE,
};

#endif /* _LINUX_GL_PLATFORM_H_ */
