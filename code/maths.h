#ifndef MATHS_H_
#define MATHS_H_

#define max(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))

#define PI32 3.1415926f

// Vector 2
struct {
    union {
        struct {
            f32 x;
            f32 y;
        };
        
        f32 e[2];
    };
} typedef v2;


// v2i
struct {
    union {
        struct {
            int x;
            int y;
        };
        
        int e[2];
    };
} typedef v2i;


// Matrix 2x2
struct {
    union {
        f32 e[2][2];
        struct {
            f32 _00, _01, _10, _11;
        };
    };
} typedef m2;

// Rect2
struct {
    v2 p[4]; // Counter-clockwise points
} typedef Rect2;


internal inline int clamp(int min, int val, int max);
internal f32 inline clampf(f32 min, f32 val, f32 max);
inline f32 lerp(f32 a, f32 t, f32 b);
inline f32 square(f32 a);
inline f32 absf(f32 a);
inline f32 map_into_range_normalized(f32 min, f32 val, f32 max);
inline f32 map_into_range_normalized_unclamped(f32 min, f32 val, f32 max);
inline f32 move_towards(f32 val, f32 target, f32 speed);
inline f32 safe_divide_1(f32 a, f32 b);
inline f32 deg_to_rad(f32 angle);
inline f32 rad_to_deg(f32 angle);
inline int trunc_f32(f32 a);
inline int ceil_f32(f32 a);

// Color
inline u32 make_color(u8 r, u8 g, u8 b);
inline u32 make_color_from_grey(u8 grey);
inline u32 lerp_color(u32 a, f32 t, u32 b);

// Vector 2
inline v2 add_v2(v2 a, v2 b);
inline v2 sub_v2(v2 a, v2 b);
inline v2 mul_v2(v2 a, f32 s);
inline f32 len_sq(v2 v);
inline f32 len_v2(v2 v);
inline v2 normalize(v2 a);
inline f32 dot_v2(v2 a, v2 b);
inline v2 lerp_v2(v2 a, f32 t, v2 b);

// v2i
inline v2i sub_v2i(v2i a, v2i b);

// Random
inline u32 random_u32();
inline b32 random_b32();
inline b32 random_choice(int chance);
inline int random_int_in_range(int min, int max); //inclusive
inline f32 random_unilateral();
inline f32 random_bilateral();
inline f32 random_f32_in_range(f32 min, f32 max);

// Matrix
v2 mul_m2_v2(m2 m, v2 v);

// Rect
inline Rect2 make_rect_min_max(v2 min, v2 max);
inline Rect2 make_rect_center_half_size(v2 c, v2 h);
inline f32 find_look_at_rotation(v2 a,  v2 b); 

#endif /* MATHS_H_ */
