// X11
//------------------------------------
// X11 shared memory extension
// It's on by default because it provides a smoother gameplay experience
// Define as 0 to disable
#ifndef X_USE_SHM
#define X_USE_SHM 1
#endif

// GL
//------------------------------------
// Use Pixel Buffer Objects to speed up copying of the framebuffer to the GPU
// Requires OpenGL 2.1 as a minimum
// Define as 1 to enable
#ifndef GL_USE_PBO
#define GL_USE_PBO 0
#endif

// SDL
//------------------------------------
// Disable runtime error checking for calls made by SDL
// By default, we check every function call made by SDL for errors
// On error, the program crashes and provides a diagnostic message upon an error
// Define as 1 to disable
#ifndef SDL_NOCHECK
#define SDL_NOCHECK 0
#endif

// Linux
//------------------------------------
// Use to profile the duration of sections of code.
// At the moment, linux and windows are supported.
// See platform_profiler.c for more details.
// Define as 1 to enable
#ifndef PLATFORM_PROFILER 
#define PLATFORM_PROFILER 0
#endif

// Game
//------------------------------------
// Use the in-game profiler
// You must call render_profiler() in your code to actually draw it
// See profiler.c for details
// Define as 1 to enable
#ifndef PROFILER
#define PROFILER 0
#endif

// Enable `developer` mode
// Up for invincibility
// Down to destroy blocks
// Right to advance to the next level
// Define as 1 to enable
#ifndef DEVELOPMENT
#define DEVELOPMENNT 0
#endif
