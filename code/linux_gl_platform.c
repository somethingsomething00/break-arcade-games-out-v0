#define LINUX_NO_SOUND
// We don't handle sound code in Linux yet,
// frankly because I have no idea how to handle sound on this platform!
// I rather crudely 'ifdefed' a bunch of code to get the rest of the game to compile, future programmers beware :<
// Some audio code may have accidentally dissapeared in the process
// On the bright side, the pixels render nicely and the input works :)


// Original game code with slight modifications
//------------------------------------------------------------------------------\/
//-------------------------------------------------------------------------------\/
//--------------------------------------------------------------------------------\/
#include "game.h"
#include "utils.c"
#include "maths.c"
#include "string.c"
#include "platform_common.c"

// Globals that were not predeclared anywhere as extern
global_variable Render_Buffer render_buffer;
global_variable f32 current_time;
global_variable b32 lock_fps = true;

// Compiler specific intrinsic
// Unused on linux
#ifdef _WIN32
#define interlocked_compare_exchange(a, b, c) InterlockedCompareExchange((volatile long*)a, b, c)
#endif

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#define STBI_NO_STDIO
#define STBI_NO_FAILURE_STRINGS
#define STBI_ASSERT assert
#include "stb_image.h"

#include "wav_importer.h"
// @Temp: No sound on Linux
// #include "ogg_importer.h"
#include "cooker_common.c"
#include "asset_loader.c"
#include "profiler.c"
#include "software_rendering.c"
#include "console.c"
// @Temp: We don't handle audio on linux yet
// #include "audio.c"
#include "game.c"
//--------------------------------------------------------------------------------\/
//--------------------------------------------------------------------------------\/
//--------------------------------------------------------------------------------\/


// Before we begin...
// Note on the os_ functions:

// These are used internally by the game, with prototypes given in platform_common.c.
// They were already a part of the game code as originally released.
// The programmer is responsible for providing a platform specific implementation.
// that follows the interface set by the prototypes.


// Note on 'inline':

// GCC was giving me a lot of grief about inline functions that lack prototypes
// so I removed most instances of the inline keyword in the project

// One may use the compiler keyword __inline to force the compiler to inline the function.

// Regarding math fucntions, I created a new header called maths.h
// that contain analagous  function prototypes to the functions implemented in maths.c.
// I also moved the definition of the structs to maths.h
// It's included in maths.c by default now.


// Note on the naming convention:

// The variable and function names following these comments are prefixed with gl_
// to indicate that they are specific to the platform layer.

// I tried to follow this convention as appropriately as possible to distinguish functions and variables that
// one may expect to provide themselves should they choose to port this code to a different platform.

// Some are purely convenience functions, but most do serve a purpose in servicing the game loop.

// Intermediate variables do not follow this convention.


// The rest is new code
// Begin OS and graphics platform specific code (Linux / GL)
//--------------------------------------------------------------------------------
/**********************************
* Platform macros
**********************************/
#define Dowhile(body) do {body} while(0)
#define Assert(cond) \
	Dowhile(if(!(cond)) {fprintf(stderr, "%s:%d: Assertion failed for expression '%s'\n", __FILE__, __LINE__, #cond); exit(1);})

#define AssertMessage(cond, ...) \
	Dowhile(if(!(cond)) {fprintf(stderr, "%s:%d: Assertion failed for expression '%s'\n", __FILE__, __LINE__, #cond); \
			fprintf(stderr, __VA_ARGS__); \
			exit(1);})

#define Pretty(...) \
	Dowhile(printf("%s:%d: ", __FILE__, __LINE__); printf(__VA_ARGS__);)

#define PrettyError(...) \
	Dowhile(printf("%s:%d: ", __FILE__, __LINE__); printf("\033[31m"); printf(__VA_ARGS__); printf("\033[0m");)

#define ArrayCount(arr_) \
	(sizeof(arr_) / sizeof(arr_)[0])

#define gl_PrintEnumString(glenum)                      \
	Dowhile(                                            \
			const GLubyte *glString;                    \
			glString = glGetString((glenum));           \
			if(glString)                                \
			{                                           \
				printf("%s: %s\n", #glenum, (glString));\
			})


// @Bug: The game hardcodes the aspect ratio to be 16:9
// Other aspect ratios will work but they do tend to look funny
#define FB_WIDTH (1280)
#define FB_HEIGHT (720)
#define PLATFORM_BYTES_PER_PIXEL 4

// Setting this to the native format that the GPU expects is crucial for performance
// Having a byte order mismatch means that the gpu has to byte swap each pixel to its native format every frame
// See https://www.khronos.org/opengl/wiki/Common_Mistakes#Slow_pixel_transfer_performance
#define PLATFORM_PREFERRED_PIXEL_FORMAT (GL_UNSIGNED_INT_8_8_8_8_REV)

// Do we want rendering to be done with pixel buffer objects?
#ifndef GL_USE_PBO
#define GL_USE_PBO 0
#endif


/**********************************
* Platform headers
**********************************/
#include "./linux_gl_platform.h"
#include "./config.h"
#include "./platform_profiler.h"
#include "./platform_profiler.c"


/**********************************
* Platform globals
**********************************/
static GLFWwindow *gl_Window;
static GLFWmonitor *gl_Monitor;
static const GLFWvidmode *gl_Vidmode;
static Input gl_Input;
static b32 gl_TimeToQuit;
static b32 gl_Paused;
static b32 gl_WasPaused;
static gl_Screen_t gl_Screen;
static b32 gl_IsFullscreen;
static gl_Texture gl_MainTexture;


/**********************************
* Platform functions
**********************************/
static void gl_CbMouseButton(GLFWwindow *Window, int button, int action, int mod);
static void gl_CbResize(GLFWwindow *Window, int x, int y);
static void gl_CbKeys(GLFWwindow *Window, int key, int scancode, int action, int mod);
static void gl_CbWinClose(GLFWwindow *Window);
static void gl_PauseGraphicDraw();
static void gl_CursorEnable(GLFWwindow *Window);
static void gl_CursorDisable(GLFWwindow *Window);
static void gl_FramebufferInit(Render_Buffer *Buffer, u32 width, u32 height, u32 bpp);
static gl_Texture gl_Texture2DCreate(u32 width, u32 height, u32 bpp, void *data);
static void gl_Texture2DDraw(GLuint id);
static void gl_Texture2DEnableAndBind(GLuint id);
static void gl_UpdateAndRenderPbo(Render_Buffer *Buffer, gl_Texture *Texture, f32 dt);
static void gl_UpdateAndRenderNoPbo(Render_Buffer *Buffer, gl_Texture *Texture, f32 dt);
static void gl_CallbackInit();
static void gl_PollKeys(GLFWwindow *Win, Input *input);
static void gl_PrintDiagnostics();




int main()
{
	// Sanity check to make sure our framebuffer is the size that we expect
	Assert(sizeof *render_buffer.pixels == PLATFORM_BYTES_PER_PIXEL);

	glfwInit();
	gl_Monitor = glfwGetPrimaryMonitor();
	gl_Vidmode = glfwGetVideoMode(gl_Monitor);
	gl_Window = glfwCreateWindow(gl_Vidmode->width, gl_Vidmode->height, "Break Arcade Games Out [LINUX GL]", gl_Monitor, NULL);


	// Some sensible screen dimension defaults
	gl_Screen.width =  gl_Vidmode->width;
	gl_Screen.height = gl_Vidmode->height;
	gl_Screen.windowedWidth = gl_Vidmode->width / 4;
	gl_Screen.windowedHeight = gl_Vidmode->height / 4;
	gl_IsFullscreen = true;
	Pretty("glfwCreateWindow: %dx%d window created\n", gl_Vidmode->width, gl_Vidmode->height);

	// 'Bind' our window
	glfwMakeContextCurrent(gl_Window);
	glfwSwapInterval(1);
	glewInit(); /* glewInit can only be called safely after called glfwMakeContextCurrent */

	// Register callbacks
	gl_CallbackInit();

	gl_FramebufferInit(&render_buffer, FB_WIDTH, FB_HEIGHT, PLATFORM_BYTES_PER_PIXEL);

	gl_MainTexture = gl_Texture2DCreate(FB_WIDTH, FB_HEIGHT, PLATFORM_BYTES_PER_PIXEL, NULL);
	gl_Texture2DEnableAndBind(gl_MainTexture.id);

	gl_CursorDisable(gl_Window);

	gl_PrintDiagnostics();

#ifdef PLATFORM_PROFILER
	u32 debug_FramesElapsed = 0;
	double debug_MsElapsed = 0;
#endif


	double t1 = 0;
	double t2 = 0;
	double dtCur = 0;
	double dtPrev = 0;
	double mouseXCurrent = 0;
	double mouseYCurrent = 0;
	double mouseXOld = 0;
	double mouseYOld = 0;
	v2i gl_MouseDp = {0};
	v2i gl_MouseDpOld = {0};

	//
	// Main loop
	//
	// glClearColor(0, 0, 0, 0);
	while(!gl_TimeToQuit)
	{
		t1 = glfwGetTime();
		glClear(GL_COLOR_BUFFER_BIT);
		glfwPollEvents();

		gl_PollKeys(gl_Window, &gl_Input);
		if(gl_Paused)
		{
			// Set the state of some things here and go into a mini game loop
			gl_MouseDpOld = gl_MouseDp;
			gl_WasPaused = true;
			gl_CursorEnable(gl_Window);
			while(gl_Paused)
			{
				glClear(GL_COLOR_BUFFER_BIT);
				if(gl_TimeToQuit)
				{
					goto the_end;
				}
				glfwWaitEvents();
				gl_Texture2DDraw(gl_MainTexture.id);
				gl_PauseGraphicDraw();
				glfwSwapBuffers(gl_Window);
				continue;
			}
			gl_CursorDisable(gl_Window);
			continue;
		}

		// Note: We poll the mouse manually to get a better feel of the transition per-frame
		// Note2: Hey it actually worked!
		glfwGetCursorPos(gl_Window, &mouseXCurrent, &mouseYCurrent);
		gl_MouseDp.x = mouseXCurrent - mouseXOld;
		gl_MouseDp.y = mouseYCurrent - mouseYOld;
		if(gl_WasPaused)
		{
			gl_Input.mouse_dp = gl_MouseDpOld;
			gl_WasPaused = false;
		}
		else
		{
			gl_Input.mouse_dp = gl_MouseDp;
		}
#if GL_USE_PBO
		gl_UpdateAndRenderPbo(&render_buffer, &gl_MainTexture, dtPrev);
#else
		gl_UpdateAndRenderNoPbo(&render_buffer, &gl_MainTexture, dtPrev);
#endif

		// This is a big hack :)
		// Since we are not tracking key repeats we need to clear the changed flag manually each frame
		// The game only acts on keys if the changed flag was in a different state on the previous frame
		#define PLATFORM_CLEAR_KEY(breakoutKey) \
			gl_Input.buttons[(breakoutKey)].changed = false

		PLATFORM_CLEAR_KEY(BUTTON_LEFT);
		PLATFORM_CLEAR_KEY(BUTTON_RIGHT);
		PLATFORM_CLEAR_KEY(BUTTON_UP);
		PLATFORM_CLEAR_KEY(BUTTON_DOWN);
		PLATFORM_CLEAR_KEY(BUTTON_ESC);
		PLATFORM_CLEAR_KEY(BUTTON_CFG);
		PLATFORM_CLEAR_KEY(BUTTON_LMB);
		mouseXOld = mouseXCurrent;
		mouseYOld = mouseYCurrent;
		
		glfwSwapBuffers(gl_Window);
		
		t2 = glfwGetTime();
		dtPrev = dtCur;
		dtCur = (t2 - t1);

#ifdef PLATFORM_PROFILER
		debug_MsElapsed += (dtPrev);
		debug_FramesElapsed++;
		if(debug_FramesElapsed == 240)
		{
			printf("%.4f ms per frame\n", (debug_MsElapsed / 240.0) * 1000.0);
			debug_FramesElapsed = 0;
			debug_MsElapsed = 0;
		}
#endif
	}
the_end:
#if !GL_USE_PBO
	free(render_buffer.pixels);
#endif
	glfwTerminate();
	return 0;
}


internal void
os_free_file(String s)
{
	free(s.data);
}

internal String
os_read_entire_file(char *file_path)
{
	String S = {0};
	FILE *F = {0};
	s64 filesize;
	s64 bytesRead;
	F = fopen(file_path, "ab+");
	Pretty("os_read_entire_file: Opening file '%s'\n", file_path);
	Assert(F != NULL);
	fseek(F, 0, SEEK_END);
	filesize = ftell(F);
	Assert(filesize != -1);
	rewind(F);
	if(filesize > 0)
	{
		S.data = calloc(filesize, sizeof *S.data);
		Assert(S.data);
		bytesRead = fread(S.data, 1, filesize, F);
		Pretty("os_read_entire_file: %ld bytes read for file '%s'\n", filesize, file_path);
	}
	S.size = filesize;
	Pretty("os_read_entire_file: %ld bytes allocated for '%s'\n", filesize, file_path);
	fclose(F);
	return S;
}

internal String
os_read_save_file()
{
	return os_read_entire_file("data/save.brk");
}

internal String
os_read_config_file()
{
	Pretty("os_read_config_file: Preparing to open config file '%s'\n", "config.txt");
	return os_read_entire_file("config.txt");
}


#define save_file_error \
	Dowhile(PrettyError("os_read_save_file: Unable to write to save file! The game will still function but progress will not be saved\n"); \
	goto save_end;)
static b32
os_write_save_file(String data)
{
	// Note on file saves: The game modifies the save contents during runtime (in a buffer)
	// When writing an updated save buffer we want to create a new file with the modified contents,
	// NOT append to the existing file (if one exists)

	FILE *TempSave = {0}; /* Write the save data to a temp file first */
	int rc;
	s64 bytesWritten;
	b32 result = true; /* We assume a successful write */
	const char *savefileName = "data/save.brk";
	const char *tempfileName = "data/save.brk.tmp";
	const char *colormodeSet = "\033[0;31m"; // @Hardcoded red color VT escape
	const char *boldSet = "\033[1m";
	TempSave =  fopen(tempfileName, "wb+");
	if(TempSave)
	{
		bytesWritten = fwrite(data.data, 1, data.size, TempSave);
		if(bytesWritten != data.size)
		{
			fclose(TempSave);
			save_file_error;
		}
	}
	else
	{
		save_file_error;
	}
	fclose(TempSave);
	rc = rename(tempfileName, savefileName);
	if(rc == 0)
	{
		Pretty("os_write_save_file: %ld bytes written to '%s'\n", bytesWritten, savefileName);
	}
	else
	{
		result = false;
		perror("rename");
		Pretty("%sUnable to rename temporary save file\n"
				"The game was successfully saved under the name %s'%s'%s\n"
				"Please navigate to its path and rename the file to %s'%s'%s to access your save file on the next launch of the game\n",
				colormodeSet,
				boldSet,tempfileName,colormodeSet,
				boldSet,savefileName,colormodeSet);
	}
save_end:
	return result;
}

String
os_get_pak_data()
{
	return os_read_entire_file("data/assets.pak");
}

internal void
os_toggle_fullscreen()
{
	// Empty
}
#define US_FACTOR 1E6
// @Hardcoded We operate in microseconds rather than seconds because last_counter truncates our time...
f32 os_seconds_elapsed(u64 last_counter)
{
	f32 tNow;
	tNow = glfwGetTime() * US_FACTOR;
	return (f32)(tNow - last_counter) / US_FACTOR;
}

u64 os_get_perf_counter()
{
	return glfwGetTime() * US_FACTOR;
}

// Note: We manually clear the changed flag in main() with PLATFORM_CLEAR_KEY
#define GLKD(glKey, breakoutKey) case (glKey): \
	gl_Input.buttons[(breakoutKey)].is_down = true; \
	gl_Input.buttons[(breakoutKey)].changed = true; \
	break

#define GLKU(glKey, breakoutKey) case (glKey): \
	gl_Input.buttons[(breakoutKey)].is_down = false; \
	gl_Input.buttons[(breakoutKey)].changed = true; \
	break
// gl_CbKeys
void gl_CbKeys(GLFWwindow *Window, int key, int scancode, int action, int mod)
{
	// Some convenience controls

	// Quit
	if((key == GLFW_KEY_Q && action == GLFW_PRESS)
	|| (key == GLFW_KEY_F4 && action == GLFW_PRESS && mod == GLFW_MOD_ALT))
	{
		gl_TimeToQuit = true;
		return;
	}

	// Pause
	if(key == GLFW_KEY_P && action == GLFW_PRESS)
	{
		gl_Paused = !gl_Paused;
	}

	// Toggle fullscreen
	if((key == GLFW_KEY_ENTER && action == GLFW_PRESS && mod == GLFW_MOD_ALT)
	|| (key == GLFW_KEY_F11 && action == GLFW_PRESS)
	|| (key == GLFW_KEY_F && action == GLFW_PRESS))
	{
		if(gl_IsFullscreen)
		{
			glfwSetWindowMonitor(gl_Window, NULL, gl_Screen.windowedX, gl_Screen.windowedY, gl_Screen.windowedWidth, gl_Screen.windowedHeight, GLFW_DONT_CARE);
			gl_IsFullscreen = 0;
		}
		else
		{
			glfwGetWindowPos(gl_Window, &gl_Screen.windowedX, &gl_Screen.windowedY);
			glfwSetWindowMonitor(gl_Window, gl_Monitor, 0, 0, gl_Vidmode->width, gl_Vidmode->height, GLFW_DONT_CARE);
			gl_IsFullscreen = 1;
		}
	}

	if(key == GLFW_KEY_R && action == GLFW_PRESS)
	{
		platform_TimedBlockClearAll();
	}

	// Provide input data needed for update_game
	switch(action)
	{
		case GLFW_PRESS:
		{
			switch(key)
			{
				GLKD(GLFW_KEY_LEFT, BUTTON_LEFT);
				GLKD(GLFW_KEY_RIGHT, BUTTON_RIGHT);
				GLKD(GLFW_KEY_UP, BUTTON_UP);
				GLKD(GLFW_KEY_DOWN, BUTTON_DOWN);
				GLKD(GLFW_KEY_ESCAPE, BUTTON_ESC);
				GLKD(GLFW_KEY_L, BUTTON_CFG);
			}
		}
		break;
		case GLFW_RELEASE:
		{
			switch(key)
			{
				GLKU(GLFW_KEY_LEFT, BUTTON_LEFT);
				GLKU(GLFW_KEY_RIGHT, BUTTON_RIGHT);
				GLKU(GLFW_KEY_UP, BUTTON_UP);
				GLKU(GLFW_KEY_DOWN, BUTTON_DOWN);
				GLKU(GLFW_KEY_ESCAPE, BUTTON_ESC);
				GLKU(GLFW_KEY_L, BUTTON_CFG);
			}
		}
		break;
	}
}

// gl_CbResize
void gl_CbResize(GLFWwindow *Window, int x, int y)
{
	glViewport(0, 0, x, y);
	gl_Screen.width = x;
	gl_Screen.height = y;
	if(!gl_IsFullscreen)
	{
		gl_Screen.windowedWidth = x;
		gl_Screen.windowedHeight = y;
	}
}


// gl_CbMouseButton
void gl_CbMouseButton(GLFWwindow *Window, int button, int action, int mod)
{
	if(button == GLFW_MOUSE_BUTTON_LEFT)
	{
		if(action == GLFW_PRESS)
		{
			gl_Input.buttons[(BUTTON_LMB)].is_down = true;
			gl_Input.buttons[(BUTTON_LMB)].changed = true;
		}
		else if (action == GLFW_RELEASE)
		{
			gl_Input.buttons[(BUTTON_LMB)].is_down = false;
			gl_Input.buttons[(BUTTON_LMB)].changed = true;
		}
	}
}

// gl_DrawWeirdGradient
void gl_DrawWeirdGradient(f32 x, f32 y, f32 w, f32 h)
{
	// Just a bit of fun

	static f32 g;
	static f32 b;

	glColor3f(0, g + 0, b + 1);
	glVertex2f(x, y);

	glColor3f(0 , g + .66, b + .33);
	glVertex2f(x + w, y);

	glColor3f(0, g + 1 , b + 0);
	glVertex2f(x + w, y + h);

	glColor3f(0, g + .33, b + .66);
	glVertex2f(x, y + h);
}


void gl_PauseGraphicDraw()
{
	glDisable(GL_TEXTURE_2D);
	glPushAttrib(GL_CURRENT_BIT);
	glPushMatrix();
	// Reset ortho to default if it wasn't
	// Not sure if these are the correct values
	glOrtho(-1, 1, -1, 1, 0, 1);
	glBegin(GL_QUADS);
	double half = 0;
	double w = 0.025;
	double h = 0.1;
	double w2 = w * 2;
	double h2 = h * 2;

	// Left side
	// Top left
	glColor3f(1,1,1);
	glVertex2f(half - w2, half - h);

	// Top right
	glColor3f(0,1,1);
	glVertex2f(half - w, half - h);

	// Bottom right
	glColor3f(0,0,1);
	glVertex2f(half - w, half + h);

	// Bottom left
	glVertex2f(half - w2, half + h);


	// Right side
	// Top left
	glColor3f(1,1,1);
	glVertex2f(half + w2, half - h);

	// Top right
	glColor3f(0,1,1);
	glVertex2f(half + w, half - h);

	// Bottom right
	glColor3f(0,0,1);
	glVertex2f(half + w, half + h);

	// Bottom left
	glVertex2f(half + w2, half + h);

	glEnd();
	glPopAttrib();
	glPopMatrix();
}

// gl_CursorEnable
void gl_CursorEnable(GLFWwindow *Window)
{
	glfwSetInputMode(Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

// gl_CursorDisable
void gl_CursorDisable(GLFWwindow *Window)
{
	glfwSetInputMode(Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

// gl_FramebufferInit
void gl_FramebufferInit(Render_Buffer *Buffer, u32 width, u32 height, u32 bpp)
{
	Assert(sizeof *Buffer->pixels == PLATFORM_BYTES_PER_PIXEL);
	Assert(bpp == PLATFORM_BYTES_PER_PIXEL);
#if !GL_USE_PBO
	Buffer->pixels = NULL;
	Buffer->pixels = malloc(width * height * bpp);
	Assert(Buffer->pixels != NULL);
#endif
	Buffer->width = width;
	Buffer->height = height;
	Pretty("gl_FramebufferInit: Initialized framebuffer -- %ux%u, %u BPP\n", width, height, PLATFORM_BYTES_PER_PIXEL);
}

// gl_CbWinClose
void gl_CbWinClose(GLFWwindow *Window)
{
	gl_TimeToQuit = true;
}

// gl_Texture2DCreate
gl_Texture gl_Texture2DCreate(u32 width, u32 height, u32 bpp, void *data)
{
	gl_Texture Texture;
	GLuint id;

	glGenTextures(1, &id);
	glGenBuffers(ArrayCount(Texture.pbo), Texture.pbo);
	glBindTexture(GL_TEXTURE_2D, id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, PLATFORM_PREFERRED_PIXEL_FORMAT, data);

	Texture.id = id;
	Texture.width = width;
	Texture.height = height;
	Texture.data = data;
	Texture.bpp = bpp;
	Texture.pboIndex = 0;
	Texture.size = width * height * bpp;
	return Texture;
}


// gl_Texture2DDraw
void gl_Texture2DDraw(GLuint id)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, id);
	glBegin(GL_QUADS);
	// Tex coord is mapped from 0, 1. X=0 is the left of the screen, Y=0 is the top of the screen,
	// Vertex is mapped from -1, 1. X=-1 is the right of the screen, Y=-1 is the bottom of the screen?

	// Top left
	glTexCoord2f(0 ,1);
	glVertex2f(-1, 1);

	// Top right
	glTexCoord2f(1, 1);
	glVertex2f(1, 1);

	// Bottom right
	glTexCoord2f(1, 0);
	glVertex2f(1, -1);

	// Bottom left
	glTexCoord2f(0, 0);
	glVertex2f(-1, -1);

	glEnd();
}

// gl_Texture2DEnableAndBind
void gl_Texture2DEnableAndBind(GLuint id)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, id);
}

// gl_UpdateAndRenderPbo
void gl_UpdateAndRenderPbo(Render_Buffer *Buffer, gl_Texture *Texture, f32 dt)
{
	u32 indexCur;
	u32 indexNext;
	GLubyte *ptr;

	Texture->pboIndex = !Texture->pboIndex;
	indexCur = Texture->pboIndex;
	indexNext = !indexCur;
	glBindTexture(GL_TEXTURE_2D, Texture->id);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, Texture->pbo[indexCur]);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, Texture->width, Texture->height, GL_BGRA, PLATFORM_PREFERRED_PIXEL_FORMAT, 0);

	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, Texture->pbo[indexNext]);
	glBufferData(GL_PIXEL_UNPACK_BUFFER, Texture->size, 0, GL_STREAM_DRAW);
	ptr = glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);
	if(ptr)
	{
		Buffer->pixels = (u32 *)ptr;
		begin_profiler();

		// Core update function provided by the game
		/**********************/
		update_game(&gl_Input, dt);
		/**********************/

		render_profiler(dt);
		glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
	}
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
	gl_Texture2DDraw(Texture->id);
}


// gl_UpdateAndRenderNoPbo
void gl_UpdateAndRenderNoPbo(Render_Buffer *Buffer, gl_Texture *Texture, f32 dt)
{
	begin_profiler();

	// Core update function provided by the game
	/**********************/
	update_game(&gl_Input, dt);
	/**********************/

	render_profiler(dt);

	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, Texture->width, Texture->height, GL_BGRA, PLATFORM_PREFERRED_PIXEL_FORMAT, Buffer->pixels);
	gl_Texture2DDraw(Texture->id);
}


// gl_CallbackInit
void gl_CallbackInit()
{
	glfwSetFramebufferSizeCallback(gl_Window, gl_CbResize);
	glfwSetKeyCallback(gl_Window, gl_CbKeys);
	glfwSetMouseButtonCallback(gl_Window, gl_CbMouseButton);
	glfwSetWindowCloseCallback(gl_Window, gl_CbWinClose);
}

// gl_PollKeys
void gl_PollKeys(GLFWwindow *Win, Input *input)
{
	if(glfwGetKey(Win, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		input->buttons[(BUTTON_DOWN)].is_down = true;
		input->buttons[(BUTTON_DOWN)].changed = true;
	}
}

// gl_PrintDiagnostics
void gl_PrintDiagnostics()
{
	printf("=====================================================================\n");
	// These only return valid values after calling glfwMakeContextCurrent
	gl_PrintEnumString(GL_VENDOR);
	gl_PrintEnumString(GL_RENDERER);
	gl_PrintEnumString(GL_VERSION);
	gl_PrintEnumString(GL_SHADING_LANGUAGE_VERSION);
	printf("Pixel buffer objects enabled: %s\n", (GL_USE_PBO) ? "Yes" : "No");
	printf("Fullscreen: %s\n", (gl_IsFullscreen) ? "Yes" : "No");
	printf("=====================================================================\n");
}
