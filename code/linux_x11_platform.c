#define LINUX_NO_SOUND
// We don't handle sound code in Linux yet,
// frankly because I have no idea how to handle sound on this platform!
// I rather crudely 'ifdefed' a bunch of code to get the rest of the game to compile, future programmers beware :<
// Some audio code may have accidentally dissapeared in the process
// On the bright side, the pixels render nicely and the input works :)

// Original game code with slight modifications
//------------------------------------------------------------------------------\/
//-------------------------------------------------------------------------------\/
//--------------------------------------------------------------------------------\/
#include "game.h"
#include "utils.c"
#include "maths.c"
#include "string.c"
#include "platform_common.c"

// Globals that were not predeclared anywhere as extern
global_variable Render_Buffer render_buffer;
global_variable f32 current_time;
global_variable b32 lock_fps = true;

// Compiler specific intrinsic
// Unused on linux
#ifdef _WIN32
#define interlocked_compare_exchange(a, b, c) InterlockedCompareExchange((volatile long*)a, b, c)
#endif

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#define STBI_NO_STDIO
#define STBI_NO_FAILURE_STRINGS
#define STBI_ASSERT assert
#include "stb_image.h"

#include "wav_importer.h"
// @Temp: No sound on Linux
// #include "ogg_importer.h"
#include "cooker_common.c"
#include "asset_loader.c"
#include "profiler.c"
#include "software_rendering.c"
#include "console.c"
// @Temp: We don't handle audio on linux yet
// #include "audio.c"
#include "game.c"
//--------------------------------------------------------------------------------\/
//--------------------------------------------------------------------------------\/
//--------------------------------------------------------------------------------\/



// Before we begin...

// Note on the os_ functions:
//--------------------------------------------------------------------------------
	// These are used internally by the game, with prototypes given in platform_common.c.
	// They were already a part of the game code as originally released.
	// The programmer is responsible for providing a platform specific implementation.
	// that follows the interface set by the prototypes.


// Note on 'inline':
//--------------------------------------------------------------------------------
	// GCC was giving me a lot of grief about inline functions that lack prototypes
	// so I removed most instances of the inline keyword in the project

	// One may use the compiler keyword __inline to force the compiler to inline the function.

	// Regarding math functions, I created a new header called maths.h
	// that contain analagous function prototypes to the functions implemented in maths.c.
	// I also moved the definition of the structs to maths.h
	// It's included in maths.c by default now.


// Note on file saves:
//--------------------------------------------------------------------------------
	// (This is already documented in os_write_save_file but there an important distinction here so I put it up here too)
	// The game modifies the save contents during runtime (in a buffer)
	// When writing an updated save buffer we want to create a new file with the modified contents,
	// NOT append to the existing file (if one exists)


//  Note on the naming convention:
//--------------------------------------------------------------------------------
	// The variable and function names following these comments are prefixed with x_
	// to indicate that they are specific to the platform layer.

	// Functions prefixed with platform_ indicate that they call to the operating system directly

	// I tried to follow this convention as appropriately as possible to distinguish functions and variables that
	// one may expect to provide themselves should they choose to port this code to a different platform.

	// Some are purely convenience functions, but most do serve a purpose in servicing the game loop.

	// Intermediate variables do not follow this convention.


//  Note on X11 Shared memory extension (X_USE_SHM define):
//--------------------------------------------------------------------------------
	// Some linux platforms support the X11 shared memory extension
	// You may control its presence in this code with the X_USE_SHM define.
	// By default it's disabled but you can enable it by passing -DX_USE_SHM=1 to the compiler.

	// Enabling the extension replaces the call of XPutImage with XShmPutImage respectively, among other things.
	// Replacing XPutImage with XShmPutImage yields a speedup from ~5ms to ~0.0019 ms respectively on a 1680x1050 framebuffer on my machine.
	// There is a cost of 1 ms extra on the memcpy in x_FramebufferSampleFromGame if using X_USE_SHM (probably) because the framebuffer memory is not as localized.
	// However it's a huge net gain overall.
	// The frame time was never in danger of dipping below 60 fps even before the introduction of this extension,
	// but it was a fun optimization to experiment with so feel free to use it if your platform supports it.



// The rest is new code
// Begin OS and graphics platform specific code (Linux / X11)
//--------------------------------------------------------------------------------
/**********************************
* Platform macros
**********************************/
#define Dowhile(body) do {body} while(0)
#define Assert(cond) \
	Dowhile(if(!(cond)) {fprintf(stderr, "%s:%d: Assertion failed for expression '%s'\n", __FILE__, __LINE__, #cond); exit(1);})

#define Unreachable(...) \
	Dowhile(\
			fprintf(stderr, "%s:%d: Unreachable: ", __FILE__, __LINE__); fprintf(stderr, __VA_ARGS__); exit(1);)

#define AssertMessage(cond, ...) \
	Dowhile(if(!(cond)) {fprintf(stderr, "%s:%d: Assertion failed for expression '%s'\n", __FILE__, __LINE__, #cond); \
			fprintf(stderr, __VA_ARGS__); \
			exit(1);})

#define Pretty(...) \
	Dowhile(printf("%s:%d: ", __FILE__, __LINE__); printf(__VA_ARGS__);)

#define PrettyError(...) \
	Dowhile(printf("%s:%d: ", __FILE__, __LINE__); printf("\033[31m"); printf(__VA_ARGS__); printf("\033[0m");)

// This sets the size of the framebuffer that the game itself renders to
// The platform has its own framebuffer that we sample to from the game framebuffer after
// it has finished updating the pixels
// 16:9 aspect ratio is optimal
#define FB_WIDTH (1280)
#define FB_HEIGHT (720)
#define PLATFORM_BYTES_PER_PIXEL 4
#define PLATFORM_EVENT_MASK \
	  KeyPressMask 		| KeyReleaseMask \
	| ButtonPressMask	| ButtonReleaseMask	\
	| EnterWindowMask	| LeaveWindowMask \
	| PointerMotionMask	| StructureNotifyMask \
	| ExposureMask		| FocusChangeMask \

#define PLATFORM_POINTER_GRAB_MASK \
	  PointerMotionMask \
	| ButtonPressMask | ButtonReleaseMask


#define PLATFORM_NS_TO_S_CONVERSION_F64 1E9
#define PLATFORM_S_TO_NS_CONVERSION_U64 1000000000ULL

#define EVENT_PROCESSMASK_BLOCK_EVENTS 0x1 /* Do we want XNextEvent to block? */
#define EVENT_PROCESSMASK_POLL_EVENTS 0x2

#define platform_MemoryRegionAssignToMember(member, size) \
	Dowhile(\
	Assert(((size) + platform_GameMemory.used) <= platform_GameMemory.totalSize); \
	platform_GameMemory.member = &platform_GameMemory.Memory[platform_GameMemory.used]; \
	platform_GameMemory.used += (size);)

#define platform_Min(a, b) (((a) < (b) ? (a) : (b)))


/**********************************
* Platform headers
**********************************/
#include "./linux_x11_platform.h"
#include "./config.h"

// Profiler
#include "./platform_profiler.h"
#include "./platform_profiler.c"

#define log(...) printf(__VA_ARGS__)

/**********************************
* Platform types
**********************************/
typedef Input input; /* Keep the naming convention consistent for the platform layer */



/**********************************
* Platform globals
**********************************/
static Window x_Window;
static b32 x_TimeToQuit;
static b32 x_IsPaused;
static b32 x_CursorIsVisible;
static b32 x_IsFullscreen;
static b32 x_WindowWasResized;
static framebuffer x_Framebuffer;
static Cursor x_Cursors[PLATFORM_CURSOR_TOTAL];
static input x_Input;
static Atom x_AtomWmDeleteWindow;
static game_memory platform_GameMemory;

// Our own keysym
static keys x_Keys = {0};


/**********************************
* Platform functions
**********************************/
static void x_FramebufferSampleFromGame(Render_Buffer *GameBuffer, framebuffer *PlatformBuffer, samplecache *Cache);
static void x_PauseGraphicDraw();
static void x_CursorRelease(Display *Dpy, Window Win);
static void x_CursorCapture(Display *Dpy, Window Win);
static void x_RenderBufferInit(Render_Buffer *Buffer, u32 width, u32 height, u32 bpp);
void x_ProcessEvents(Display *Dpy, Window Win, input *Input, screeninfo *ScreenInfo, mouse *Mouse, keys *Keys, process_mask_t Eventmask);
void x_Eventfunc(Display *Dpy, Window Win, input *Input, screeninfo *ScreenInfo, mouse *Mouse, keys *Keys);
static void x_SyncFramebufferToWindowDimensions(framebuffer *Framebuffer, screeninfo *ScreenInfo);
static XRRScreenSize *x_QuerySupportedMonitorResolutions(Display *Dpy, int screenNum, int *nsizes);
static void x_CursorsInit(Display *Dpy, Window Win);
static u64 x_MonitorRefreshRateToNs(u64 rateInHz);
static void x_BufferSampleCacheGenerate(samplecache *Cache, u32 gameFbWidth, u32 gameFbHeight, u32 platformFbWidth, u32 platformFbHeight);
static void x_WindowTitleSet(Display *Dpy, Window Win, const char *title);
void x_WindowToggleFullscreen(Display *Dpy, Window Win);

static void x_ProcessKeysm(keys *Keys, input *Input, Display *Dpy);
static b32 x_KeysymIsDown(const keys *Keys, Display *Dpy, const u32 Code);
static b32 x_KeysymIsUp(const keys *Keys, Display *Dpy, const u32 Code);
static b32 x_KeysymIsHeld(const keys *Keys, Display *Dpy, const u32 Code);
static void x_KeysymSetToHeld(keys *Keys, Display *Dpy, const u32 Code);
static void x_ProcessGameButtonAsSinglePress(keys *Keys, input *Input, Display *Dpy,  u32 PlatformButton, game_button GameButton);
static void x_ProcessGameButtonAsHeld(keys *Keys, input *Input, Display *Dpy, u32 PlatformButton, game_button GameButton);

int ListProperties(Display *Dpy, Window Win);
static b32 x_AtomIsPresent(Display *Dpy, const char *atomName);
static b32 x_WindowAtomIsPresent(Display *Dpy, Window Win, const char *atomName, const char *subatomName);
static b32 x_AtomContains(Display *Dpy, unsigned long *pAtom, long unsigned int nItems, const char *subatomName);


static u64 platform_GetTimeNs();
static void platform_SleepNs(u64 ns);
static void platform_Vsync(u64 nsElapsed, u64 nsTarget);
static void *platform_MemoryMap(u64 bytes);
static void platform_MemoryUnmap(void *ptr, u64 bytes);
static u8 platform_Strlen255(const char *s);


// Do we want to use the X11 shared memory extension?
#ifndef X_USE_SHM
#define X_USE_SHM 0
#endif

#if X_USE_SHM
#include "linux_x11_shm.h"
#include "linux_x11_shm.c"
#endif

// Conditional macros based on which API we use to get and display an XImage
// It's pretty ugly but it was better than having a bunch of ifdefs in the game loop itself

// The X_USE_SHM path uses the X11 XShm extension for memory and putting the image to the screen
// The regular path uses a regular memory allocation and a XPutImage call for putting the image to the screen


#if X_USE_SHM
#define x_StuffOnWindowResize()                                                                          \
	Dowhile(                                                                                             \
	if(x_WindowWasResized)                                                                               \
	{                                                                                                    \
		x_WindowWasResized = false;                                                                      \
		x_SyncFramebufferToWindowDimensions(&x_DoubleBuffer.Front.Fb, &x_ScreenInfo);                    \
		x_SyncFramebufferToWindowDimensions(&x_DoubleBuffer.Back.Fb, &x_ScreenInfo);                     \
		XGetWindowAttributes(x_Display, x_Window, &x_Attrs);                                             \
		x_DoublebufferResetOnResize(&x_DoubleBuffer);                                                    \
		x_BufferSampleCacheGenerate(&x_BufferSampleCache, FB_WIDTH, FB_HEIGHT,                           \
									x_DoubleBuffer.Front.Fb.subwidth, x_DoubleBuffer.Front.Fb.subheight);\
	})

#define x_FramebufferSample() \
	Dowhile(\
	x_FramebufferSampleFromGame(&render_buffer, &x_DoubleBuffer.Front.Fb, &x_BufferSampleCache);)

#define x_ImagePut() \
	Dowhile(\
	XShmPutImage(x_Display, x_Window, x_Gc, x_DoubleBuffer.Front.Image, 0, 0, 0, 0,\
							x_DoubleBuffer.Front.Fb.subwidth, x_DoubleBuffer.Front.Fb.subheight, 0);)

// ---------------------------------------------
#else
// ---------------------------------------------

#define x_StuffOnWindowResize()                                                     \
	Dowhile(                                                                        \
	if(x_WindowWasResized)                                                          \
	{                                                                               \
		x_WindowWasResized = false;                                                 \
		x_SyncFramebufferToWindowDimensions(&x_Framebuffer, &x_ScreenInfo);         \
		XFree(x_Image);                                                             \
		XGetWindowAttributes(x_Display, x_Window, &x_Attrs);                        \
		x_Image = XCreateImage(x_Display, x_Attrs.visual, x_Attrs.depth, ZPixmap, 0,\
		(char *)x_Framebuffer.data,                                                 \
		x_Framebuffer.subwidth, x_Framebuffer.subheight,                            \
		32, x_Framebuffer.subwidth * sizeof(framebuffer_t));                        \
		x_BufferSampleCacheGenerate(&x_BufferSampleCache, FB_WIDTH, FB_HEIGHT,      \
				x_Framebuffer.subwidth, x_Framebuffer.subheight);                   \
	})

#define x_FramebufferSample() \
	Dowhile(\
	x_FramebufferSampleFromGame(&render_buffer, &x_Framebuffer, &x_BufferSampleCache);)

#define x_ImagePut() \
	Dowhile(\
	XPutImage(x_Display, x_Window, x_Gc, x_Image, 0, 0, 0, 0, x_Framebuffer.subwidth, x_Framebuffer.subheight);)

#endif /* X_USE_SHM */

int main()
{
/**********************************
 * Platform internals
 **********************************/
	XRRScreenConfiguration *x_ScreenConfig = NULL;
	XWindowAttributes x_Attrs = {0};
	XRRScreenSize *x_SupportedScreenSizes = {0};

	v2i x_MouseDp = {0};
	v2i x_MouseDpOld = {0};
	mouse x_Mouse = {0};
	s32 mouseXCurrent = 0;
	s32 mouseYCurrent = 0;
	s32 mouseXOld = 0;
	s32 mouseYOld = 0;

	u64 x_MonitorRefreshRateInNs = 0;
	samplecache x_BufferSampleCache = {0};

	Window x_RootWindow = 0;
	screeninfo x_ScreenInfo = {0};
	int x_ScreenNum = 0;
	GC x_Gc = 0;
	Display *x_Display = NULL;
	Screen *x_Screen = NULL;

	// Will hold the number of supported screen resolutions on the platform
	int sizes;

	// Sanity check that our sizes match with what the game expects
	Assert(sizeof *x_Framebuffer.data == sizeof(framebuffer_t));
	Assert(sizeof *x_Framebuffer.data == PLATFORM_BYTES_PER_PIXEL);
	Assert(sizeof *render_buffer.pixels == PLATFORM_BYTES_PER_PIXEL);

	x_Display = XOpenDisplay(NULL);
	Assert(x_Display != NULL);
	x_RootWindow = DefaultRootWindow(x_Display);
	x_ScreenNum = DefaultScreen(x_Display);
	x_Screen = DefaultScreenOfDisplay(x_Display);

	x_Window = XCreateSimpleWindow(x_Display, x_RootWindow, 0, 0, x_Screen->width, x_Screen->height, 0, 0, 0);
	x_WindowTitleSet(x_Display, x_Window, "Break Arcade Games Out [LINUX X11]");

	XGetWindowAttributes(x_Display, x_Window, &x_Attrs);
	x_Gc = XCreateGC(x_Display, x_Window, 0, NULL);
	x_ScreenConfig = XRRGetScreenInfo(x_Display, x_Window); /* Note: RandR extension */
	Assert(x_ScreenConfig != NULL);

	XSelectInput(x_Display, x_Window, PLATFORM_EVENT_MASK);

	// Respond to screen changes with xrandr
	XRRSelectInput(x_Display, x_RootWindow, RRScreenChangeNotifyMask);

	// Some sensible screen dimension defaults
	x_ScreenInfo.width =  x_Screen->width;
	x_ScreenInfo.height = x_Screen->height;
	x_ScreenInfo.widthOld =  x_Screen->width;
	x_ScreenInfo.heightOld = x_Screen->height;
	x_ScreenInfo.fullscreenWidth = x_Screen->width;
	x_ScreenInfo.fullscreenHeight = x_Screen->height;
	x_ScreenInfo.refreshRate = XRRConfigCurrentRate(x_ScreenConfig);
	x_IsFullscreen = true;

	// Set up frame limiter
	x_MonitorRefreshRateInNs = x_MonitorRefreshRateToNs(x_ScreenInfo.refreshRate);

	Pretty("XCreateSimpleWindow: %dx%d window created\n", x_Screen->width, x_Screen->height);
	Pretty("Fullscreen: %s\n", (x_IsFullscreen) ? "Yes" : "No");
	Pretty("XRRConfigCurrentRate: Refresh rate of %d hz\n", x_ScreenInfo.refreshRate);

	// Event that fires when the 'close' button is pressed on a traditional window manager
	if((x_AtomWmDeleteWindow = XInternAtom(x_Display, "WM_DELETE_WINDOW", False)))
	{
		XSetWMProtocols(x_Display, x_Window, &x_AtomWmDeleteWindow, 1);
	}

	// This will ensure that only one event pair of keyup/keydown is ever generated for any keypress
	// By default X11 will generate keydown events on autorepeat events as well, which we don't want
	XkbSetDetectableAutoRepeat(x_Display, True, NULL);


	// x_SupportedScreenSizes will contain an array of structs containing the total number of resolutions supported by the platform
	// I am assuming that index 0 of the returned structs will contain the largest resolution supported by the monitor / platform
	// We will allocate a buffer of the largest size to use for resize events, so we don't have to free and reallocate.
	// It's fine if we render onto a subset of the platform buffer
	x_SupportedScreenSizes = XRRConfigSizes(x_ScreenConfig, &sizes);
	x_Framebuffer.width = x_SupportedScreenSizes[0].width;
	x_Framebuffer.height = x_SupportedScreenSizes[0].height;
	x_Framebuffer.subwidth = x_Framebuffer.width;
	x_Framebuffer.subheight = x_Framebuffer.height;
	x_RenderBufferInit(&render_buffer, FB_WIDTH, FB_HEIGHT, PLATFORM_BYTES_PER_PIXEL);

	x_ScreenInfo.widthNative = x_SupportedScreenSizes[0].width;
	x_ScreenInfo.heightNative = x_SupportedScreenSizes[0].height;

	// Memory arena stuff
	// Allocate memory arena and distribute memory to it
	// This bit of code was mostly a small exercise in writing a basic memory arena
	// As a result of how mmap works this renders the code very inflexible to screen resolution changes
	// For instance, a new monitor was plugged in that has a higher max-resolution that the one that the game started with
	// Now we need to unmap the entire region and reallocate it rather than reallocate only the bits we need
	u64 desiredMemoryArenaSizeInBytes = 0;
	u64 platformFramebufferSize = 0;
	u64 gameFramebufferSize = 0;
	u64 samplecacheXsSize = 0;
	u64 samplecacheYsSize = 0;

	gameFramebufferSize = render_buffer.width * render_buffer.height * sizeof(framebuffer_t);
	platformFramebufferSize = x_Framebuffer.width * x_Framebuffer.height * sizeof(framebuffer_t);
	samplecacheXsSize = x_Framebuffer.width * sizeof *x_BufferSampleCache.xs;
	samplecacheYsSize = x_Framebuffer.height * sizeof *x_BufferSampleCache.ys;

	desiredMemoryArenaSizeInBytes = gameFramebufferSize + platformFramebufferSize + samplecacheXsSize + samplecacheYsSize;
	platform_GameMemory.Memory = platform_MemoryMap(desiredMemoryArenaSizeInBytes);
	platform_GameMemory.totalSize = desiredMemoryArenaSizeInBytes;
	Pretty("Initialized platform memory arena. %lu bytes allocated (%.2f MiB)\n",
	platform_GameMemory.totalSize, (f32)platform_GameMemory.totalSize / (f32)(1 << 20));

	// Validate that the memory regions have enough space and assign contigous addresses to the members
	platform_MemoryRegionAssignToMember(FramebufferPlatform, platformFramebufferSize);
	platform_MemoryRegionAssignToMember(FramebufferGame, gameFramebufferSize);
	platform_MemoryRegionAssignToMember(SampleCacheXs, samplecacheXsSize);
	platform_MemoryRegionAssignToMember(SampleCacheYs, samplecacheYsSize);

	// Distribute
	render_buffer.pixels = platform_GameMemory.FramebufferGame;
	x_Framebuffer.data = platform_GameMemory.FramebufferPlatform;
	x_BufferSampleCache.xs = platform_GameMemory.SampleCacheXs;
	x_BufferSampleCache.ys = platform_GameMemory.SampleCacheYs;

	Pretty("Distributed platform memory arena members.\n");
	Pretty("Initialized platform render buffer. %dx%d allocated. %dx%d visible\n",
			x_Framebuffer.width, x_Framebuffer.height, x_Framebuffer.subwidth, x_Framebuffer.subheight);

	x_BufferSampleCacheGenerate(&x_BufferSampleCache, FB_WIDTH, FB_HEIGHT, x_Framebuffer.width, x_Framebuffer.height);
	x_CursorsInit(x_Display, x_Window);

	// Display our window!
	XMapWindow(x_Display, x_Window);

	// Get rid of borders
	// @Bug: This doesn't do what we want until we resize the window :(
	XWindowChanges Changes = {0};
	XConfigureWindow(x_Display, x_Window, CWBorderWidth, &Changes);
	for(;;)
	{
		XEvent Event;
		XNextEvent(x_Display, &Event);
		if(Event.type == MapNotify) break;
	}


#if X_USE_SHM
	// The XSHM extension submits our data so quickly that double buffering is a necessity
	doublebuffer x_DoubleBuffer = {0};
	x_DoublebufferCreate(&x_DoubleBuffer, x_Display, &x_Attrs, x_Framebuffer.width, x_Framebuffer.height);
	Pretty("Rendering implementation: XShm\n");
#else
	// Regular x-image
	// Create a full size image buffer
	XImage *x_Image = {0};
	x_Image = XCreateImage(x_Display, x_Attrs.visual, x_Attrs.depth, ZPixmap, 0,
	(char *)x_Framebuffer.data, x_Framebuffer.width, x_Framebuffer.height, 32, x_Framebuffer.width * sizeof(framebuffer_t));
	Assert(x_Image != NULL);
	Pretty("Rendering implementation: XImage\n");
#endif
	x_CursorCapture(x_Display, x_Window);
	XMoveResizeWindow(x_Display, x_Window, 0, 0, x_Screen->width, x_Screen->height);
#if PLATFORM_PROFILER
	u32 debug_FramesElapsed = 0;
	f64 debug_MsElapsed = 0;
	u64 debug_TimeBeforeVsync = 0;
#endif
	u64 t1 = 0;
	u64 t2 = 0;
	u64 dtCur = 0;
	u64 dtPrev = 0;

	// Check that the Window Manager supports the extended window manager hints we want
	// Note: we don't have a fallback path if this fails (XMoveResizeWindow...)
	Assert(x_AtomIsPresent(x_Display, "_NET_WM_STATE"));
	Assert(x_AtomIsPresent(x_Display, "_NET_WM_STATE_FULLSCREEN"));
	x_WindowToggleFullscreen(x_Display, x_Window);

	//
	// Main loop
	//
	while(!x_TimeToQuit)
	{
		t1 = platform_GetTimeNs();
		begin_profiler();
		x_ProcessEvents(x_Display, x_Window, &x_Input, &x_ScreenInfo, &x_Mouse, &x_Keys, EVENT_PROCESSMASK_POLL_EVENTS);
		if(x_IsPaused)
		{
			// Set the state of some things here and go into a mini game loop
			x_MouseDpOld = x_MouseDp;
			x_CursorRelease(x_Display, x_Window);
			while(x_IsPaused)
			{
				if(x_TimeToQuit)
				{
					goto the_end;
				}
				x_ProcessEvents(x_Display, x_Window, &x_Input, &x_ScreenInfo, &x_Mouse, &x_Keys, EVENT_PROCESSMASK_BLOCK_EVENTS);
				x_PauseGraphicDraw();
				x_StuffOnWindowResize();
				x_FramebufferSample();
				x_ImagePut();
				continue;
			}
			x_CursorCapture(x_Display, x_Window);
			continue;
		}
		mouseXCurrent = x_Mouse.xTotal;
		mouseYCurrent = x_Mouse.yCur;
		x_MouseDp.x = mouseXCurrent - mouseXOld;
		x_MouseDp.y = mouseYCurrent - mouseYOld;
		mouseXOld = mouseXCurrent;
		mouseYOld = mouseYCurrent;
		x_Input.mouse_dp = x_MouseDp;

		platform_TimedBlockBegin(TB_GAME);

		// Core update function
		//---------------------------------------------
		update_game(&x_Input, (f64)dtPrev / PLATFORM_NS_TO_S_CONVERSION_F64);
		//---------------------------------------------

		platform_TimedBlockEnd(TB_GAME);

		render_profiler((f64)dtPrev / PLATFORM_NS_TO_S_CONVERSION_F64);
		x_StuffOnWindowResize();
		platform_TimedBlockBegin(TB_MEMCPY);
		x_FramebufferSample();
		platform_TimedBlockEnd(TB_MEMCPY);
		x_ImagePut();

		t2 = platform_GetTimeNs();
		dtCur = (t2 - t1);
		dtPrev = dtCur;
#if PLATFORM_PROFILER
		debug_TimeBeforeVsync = dtCur; /* Get the real frame time before the frame limiter kicks in */
#endif
#if X_USE_SHM
		x_SwapBuffers(&x_DoubleBuffer);
#endif
		platform_Vsync(dtPrev, x_MonitorRefreshRateInNs);
		t2 = platform_GetTimeNs();
		dtCur = (t2 - t1);
		dtPrev = dtCur;
		// platform_CycleBlockList();

#if PLATFORM_PROFILER
		debug_MsElapsed += ((f64)debug_TimeBeforeVsync / 1E9);
		debug_FramesElapsed++;
		if(debug_FramesElapsed == 240)
		{
			printf("%.4f ms per frame\n", (debug_MsElapsed / 240.0) * 1000.0);
			debug_FramesElapsed = 0;
			debug_MsElapsed = 0;
		}
		// platform_CycleBlockList();
#endif
	}
the_end:
	Pretty("Cleaning up...\n");
#if X_USE_SHM
	x_DoublebufferCleanup(&x_DoubleBuffer, x_Display);
#else
	XFree(x_Image);
#endif
	XRRFreeScreenConfigInfo(x_ScreenConfig);
	XCloseDisplay(x_Display);
	platform_MemoryUnmap(platform_GameMemory.Memory, platform_GameMemory.totalSize);
	platform_TimedBlockList();
	Pretty("Exiting...\n");
	return 0;
}

// os_* functions
// These are implementations for the game to use
//--------------------------------------------------------------------------------
static void os_free_file(String s)
{
	free(s.data);
}

static String os_read_entire_file(char *file_path)
{
	String S = {0};
	FILE *F = {0};
	s64 filesize;
	s64 bytesRead;
	F = fopen(file_path, "ab+");
	Pretty("os_read_entire_file: Opening file '%s'\n", file_path);
	Assert(F != NULL);
	fseek(F, 0, SEEK_END);
	filesize = ftell(F);
	Assert(filesize != -1);
	rewind(F);
	if(filesize > 0)
	{
		S.data = calloc(filesize, sizeof *S.data);
		Assert(S.data);
		bytesRead = fread(S.data, 1, filesize, F);
		Pretty("os_read_entire_file: %ld bytes read for file '%s'\n", filesize, file_path);
	}
	S.size = filesize;
	Pretty("os_read_entire_file: %ld bytes allocated for '%s'\n", filesize, file_path);
	fclose(F);
	return S;
}

static String os_read_save_file()
{
	return os_read_entire_file("data/save.brk");
}

static String os_read_config_file()
{
	Pretty("os_read_config_file: Preparing to open config file '%s'\n", "config.txt");
	return os_read_entire_file("config.txt");
}


#define save_file_error \
	Dowhile(PrettyError("os_read_save_file: Unable to write to save file! The game will still function but progress will not be saved\n"); \
	goto save_end;)
static b32 os_write_save_file(String data)
{
	// Note on file saves: The game modifies the save contents during runtime (in a buffer)
	// When writing an updated save buffer we want to create a new file with the modified contents,
	// NOT append to the existing file (if one exists)

	FILE *TempSave = {0}; /* Write the save data to a temp file first */
	int rc;
	s64 bytesWritten;
	b32 result = true; /* We assume a successful write */
	const char *savefileName = "data/save.brk";
	const char *tempfileName = "data/save.brk.tmp";
	const char *colormodeSet = "\033[0;31m"; // @Hardcoded red color VT escape
	const char *boldSet = "\033[1m";
	TempSave =  fopen(tempfileName, "wb+");
	if(TempSave)
	{
		bytesWritten = fwrite(data.data, 1, data.size, TempSave);
		if(bytesWritten != data.size)
		{
			fclose(TempSave);
			save_file_error;
		}
	}
	else
	{
		save_file_error;
	}
	fclose(TempSave);
	rc = rename(tempfileName, savefileName);
	if(rc == 0)
	{
		Pretty("os_write_save_file: %ld bytes written to '%s'\n", bytesWritten, savefileName);
	}
	else
	{
		result = false;
		perror("rename");
		Pretty("%sUnable to rename temporary save file\n"
				"The game was successfully saved under the name %s'%s'%s\n"
				"Please navigate to its path and rename the file to %s'%s'%s to access your save file on the next launch of the game\n",
				colormodeSet,
				boldSet,tempfileName,colormodeSet,
				boldSet,savefileName,colormodeSet);
	}
save_end:
	return result;
}

String os_get_pak_data()
{
	return os_read_entire_file("data/assets.pak");
}

static void os_toggle_fullscreen()
{
	// Empty
}

f32 os_seconds_elapsed(u64 last_counter)
{
	u64 tNow;
	tNow = platform_GetTimeNs();
	return (tNow - last_counter) / 1E9;
}

u64 os_get_perf_counter()
{
	return platform_GetTimeNs();
}

// x_* functions
//--------------------------------------------------------------------------------
void x_DrawWeirdGradient(u32 x, u32 y, u32 w, u32 h)
{
	u32 xBegin;
	u32 xEnd;
	u32 yEnd;
	f32 b;
	f32 g;
	f32 bStep;
	f32 gStep;
	f32 bBegin;
	f32 gBegin;
	u32 bEnd;
	u32 gEnd;
	u32 ySteps;
	u32 xSteps;

	xBegin = x;
	xEnd = x + w;
	yEnd = y + h;

	bBegin = 60;
	gBegin = 60;

	bEnd = 255;
	gEnd = 255;

	ySteps = yEnd - y;
	xSteps = xEnd - xBegin;

	bStep = (bEnd - bBegin) / xSteps;
	gStep = (gEnd - gBegin) / ySteps;

	g = gBegin;
	while(y < yEnd)
	{
		x = xBegin;
		b = bBegin;
		while(x < xEnd)
		{
			render_buffer.pixels[y * FB_WIDTH + x] = (u32)b | ((u32)g << 8);
			b += bStep;
			x++;
		}
		g += gStep;
		y++;
	}
}

void x_PauseGraphicDraw()
{
	f32 w;
	f32 h;
	f32 x1;
	f32 y1;
	f32 x2;
	f32 y2;
	h = (f32)FB_HEIGHT * .1;
	w = (f32)FB_WIDTH * .015;
	x1 = (FB_WIDTH * .5) - (w + (w / 2));
	y1 = (FB_HEIGHT * .5) - (h / 2);
	x2 = (FB_WIDTH * .5) + (w - (w / 2));
	y2 = (FB_HEIGHT * .5) - (h / 2);

	x_DrawWeirdGradient(x1, y1, w, h);
	x_DrawWeirdGradient(x2, y2, w, h);
}

void x_CursorRelease(Display *Dpy, Window Win)
{
	XDefineCursor(Dpy, Win, x_Cursors[PLATFORM_CURSOR_VISIBLE]);
	XUngrabPointer(Dpy, CurrentTime);
	x_CursorIsVisible = true;
}

void x_CursorCapture(Display *Dpy, Window Win)
{
	XDefineCursor(Dpy, Win, x_Cursors[PLATFORM_CURSOR_INVISIBLE]);
	XGrabPointer(Dpy, Win, True, PLATFORM_POINTER_GRAB_MASK, GrabModeAsync, GrabModeAsync, Win, None, CurrentTime);
	x_CursorIsVisible = false;
}

void x_RenderBufferInit(Render_Buffer *Buffer, u32 width, u32 height, u32 bpp)
{
	Buffer->width = width;
	Buffer->height = height;
	Pretty("x_RenderBufferInit: Initialized game render buffer -- %ux%u, %u BPP\n", width, height, bpp);
}


#define PLATFORM_MODMASK_ALT Mod1Mask
#define PLATFORM_MODMASK_CTRL ControlMask
void x_Eventfunc(Display *Dpy, Window Win, input *Input, screeninfo *ScreenInfo, mouse *Mouse, keys *Keys)
{
	XEvent Event = {0};
	KeySym Sym;

	XNextEvent(Dpy, &Event);

	switch(Event.type)
	{
		case ClientMessage:
		{
			if((Atom)Event.xclient.data.l[0] == x_AtomWmDeleteWindow)
				x_TimeToQuit = 1;
		} break;

		// Resolution was changed
		// @Hardcoded: This is an event number that fires for a resolution change in the Xrandr extension
		// We have have no idea how to retrive this event properly. This number was deduced by trial and printf.
		case 89:
		{
			XRRScreenChangeNotifyEvent xre;
			xre = *(XRRScreenChangeNotifyEvent *)&Event;
			ScreenInfo->fullscreenWidth = xre.width;
			ScreenInfo->fullscreenHeight = xre.height;
			if(x_IsFullscreen)
			{
				ScreenInfo->width = xre.width;
				ScreenInfo->height = xre.height;
				log("(Event 89 in x_Eventfunc) On fullscreen, set dimensions to %dx%d\n", ScreenInfo->width, ScreenInfo->height);
			}
			x_WindowWasResized = true;
		} break;

		case KeyPress:
		{
			if(!Keys->Key[Event.xkey.keycode].isDown
			&& !Keys->Key[Event.xkey.keycode].isHeld)
			{
				Keys->Key[Event.xkey.keycode].isDown = true;
			}
			Sym = XkbKeycodeToKeysym(Dpy, Event.xkey.keycode, 0, 0);
			switch(Sym)
			{
				// Platform layer controls
				// The only key controls that are explicitly handled by X11, since we don't track modifier keys in our own keysym

				// Quit
				case XK_F4:
				{
					if(Event.xkey.state & PLATFORM_MODMASK_ALT)
					{
						x_TimeToQuit = 1;
						return;
					}
				} break;

				// Toggle fullscreen
				case XK_f:
				case XK_Return:
				{
					if(Sym == XK_Return)
					{
						if(!(Event.xkey.state & PLATFORM_MODMASK_ALT))
						{
							break;
						}
					}
					// This is dumb and pretty inefficient but it's an easy way to catch an error in our code
					if(x_IsFullscreen)
					{
						if(!x_WindowAtomIsPresent(Dpy, x_Window, "_NET_WM_STATE", "_NET_WM_STATE_FULLSCREEN"))
						{
							Unreachable("The fullscreen flag was set but we did not set the window properties to contain the relevant atoms!"
									" Expected atoms '%s' and '%s' to be present.\n", "_NET_WM_STATE", "_NET_WM_STATE_FULLSCREEN");
						}
					}
					x_IsFullscreen = !x_IsFullscreen;

					x_WindowToggleFullscreen(Dpy, Win);
					x_WindowWasResized = true;
					if(!x_IsFullscreen)
					{
						// We would get our data from ConfigureNotify but this looks nicer during the transition to windowed mode
						ScreenInfo->width = ScreenInfo->widthOld;
						ScreenInfo->height = ScreenInfo->heightOld;
					}
					else
					{
						XWindowAttributes Attributes;
						XGetWindowAttributes(Dpy, Win, &Attributes);
						ScreenInfo->widthOld = ScreenInfo->width;
						ScreenInfo->heightOld = ScreenInfo->height;
						ScreenInfo->width = ScreenInfo->fullscreenWidth;
						ScreenInfo->height = ScreenInfo->fullscreenHeight;

						// The window manager should remember where our window was, but we save it just in case
						ScreenInfo->xposOld = Attributes.x;
						ScreenInfo->yposOld = Attributes.y;
					}

				} break;

			}
		} break;

		case KeyRelease:
		{
			Keys->Key[Event.xkey.keycode].isDown = false;
			Keys->Key[Event.xkey.keycode].isHeld = false;
		} break;

		case MotionNotify:
		{
			// Mouse cursor

			// @Hack: We check the warped condition on every mouse move event
			// There must be a better way of doing this
			static int warped = 0;
			if(!warped)
			{
				Mouse->xOld = Mouse->xCur;
				Mouse->yOld = Mouse->yCur;
				Mouse->xCur = Event.xmotion.x;
				Mouse->yCur = Event.xmotion.y;
			}
			else
			{
				warped = 0;
			}
			if(!x_IsPaused)
			{
				Mouse->xTotal += (Mouse->xCur - Mouse->xOld);

				// Set up infinite scroll (X axis only for now)
				if(Event.xmotion.x <= 0 || (unsigned)Event.xmotion.x >= ScreenInfo->width - 1) /* Note the -1 is important */
				{
					s32 xTemp;
					xTemp = Mouse->xCur;
					Mouse->xCur = ScreenInfo->width / 2;
					// Thanks to Suckless-warp for this bit of code regarding calling XWarpPointer properly
					XWarpPointer(Dpy, None, Win, 0, 0, 0, 0, Mouse->xCur, Mouse->yCur);
					Mouse->xOld = (xTemp - Mouse->xOld) + Mouse->xCur;
					warped = 1;
				}
			}

		} break;

		case ButtonPress:
		{
			switch(Event.xbutton.button)
			{
				// Left mouse
				case Button1:
				{
					Input->buttons[BUTTON_LMB].is_down = true;
					Input->buttons[BUTTON_LMB].changed = true;
				} break;
			}
		} break;

		case ButtonRelease:
		{
			switch(Event.xbutton.button)
			{
				// Left mouse
				case Button1:
				{
					Input->buttons[BUTTON_LMB].is_down = false;
					Input->buttons[BUTTON_LMB].changed = true;
				} break;
			}
		} break;

		// This should capture resize events
		// Could be either Expose or ConfigureNotify
		case ConfigureNotify:
		{
			int w;
			int h;
			w = Event.xconfigure.width;
			h = Event.xconfigure.height;
			x_WindowWasResized = true;

			if(!x_IsFullscreen)
			{
				ScreenInfo->xpos = Event.xconfigure.x;
				ScreenInfo->ypos = Event.xconfigure.y;
				ScreenInfo->width = Event.xconfigure.width;
				ScreenInfo->height = Event.xconfigure.height;
			}
		} break;

		// Window focus
		case FocusOut:
		{
			x_CursorRelease(Dpy, Win);
		} break;

		case FocusIn:
		{
			if(!x_IsPaused)
			{
				x_CursorCapture(Dpy, Win);
			}
		} break;
	}
}

void x_ProcessEvents(Display *Dpy, Window Win, input *Input, screeninfo *ScreenInfo, mouse *Mouse, keys *Keys, process_mask_t Eventmask)
{
	if(Eventmask & EVENT_PROCESSMASK_POLL_EVENTS)
	{
		while(XPending(Dpy))
		{
			x_Eventfunc(Dpy, Win, Input, ScreenInfo, Mouse, Keys);
		}
		x_ProcessKeysm(Keys, Input, Dpy);
	}
	// Let XNextEvent block for us
	// Useful when pausing, for example
	else if(Eventmask & EVENT_PROCESSMASK_BLOCK_EVENTS)
	{
		while(XPending(Dpy))
		{
			x_Eventfunc(Dpy, Win, Input, ScreenInfo, Mouse, Keys);
		}
		x_Eventfunc(Dpy, Win, Input, ScreenInfo, Mouse, Keys); /* This will block */
		x_ProcessKeysm(Keys, Input, Dpy);
	}
}


XRRScreenSize *x_QuerySupportedMonitorResolutions(Display *Dpy, int screenNum, int *nsizes)
{
	XRRScreenSize *Result;
	Result = XRRSizes(Dpy, screenNum, nsizes);
	Assert(Result != NULL);
	return Result;
}

void x_SyncFramebufferToWindowDimensions(framebuffer *Framebuffer, screeninfo *ScreenInfo)
{
	// @Hack: We dont reallocate the platform framebuffer if it's larger than the largest native resolution
	// The framebuffer width and height are initialized to be the largest native resolution on startup.
	// So if the window exceeds this, we clamp it according to the framebuffer, not the window.
	Framebuffer->subwidth = platform_Min(ScreenInfo->width, Framebuffer->width);
	Framebuffer->subheight = platform_Min(ScreenInfo->height, Framebuffer->height);
}

void x_CursorsInit(Display *Dpy, Window Win)
{
	// Thanks to rck from
	// https://stackoverflow.com/questions/660613/how-do-you-hide-the-mouse-pointer-under-linux-x11
	// for explaining how to create a blank mouse cursor
	Pixmap bitmapNoData;
	char noData[8] = {0};
	XColor blackColor = {0};
	bitmapNoData = XCreateBitmapFromData(Dpy, Win, noData, 8, 8);
	x_Cursors[PLATFORM_CURSOR_INVISIBLE] = XCreatePixmapCursor(Dpy, bitmapNoData, bitmapNoData, &blackColor, &blackColor, 0, 0);
	x_Cursors[PLATFORM_CURSOR_VISIBLE] = XCreateFontCursor(Dpy,XC_left_ptr);
	XFreePixmap(Dpy, bitmapNoData);
}


void x_BufferSampleCacheGenerate(samplecache *Cache, u32 gameFbWidth, u32 gameFbHeight, u32 platformFbWidth, u32 platformFbHeight)
{
	u32 x;
	u32 y;
	// The game draws its pixels backwards to our rendering scheme so we reverse the lookup order
	for(y = 0; y < platformFbHeight; y++)
	{
		Cache->ys[y] = ((f64)(platformFbHeight - y - 1) / (f64)platformFbHeight) * (f64)gameFbHeight;
	}

	for(x = 0; x < platformFbWidth; x++)
	{
		Cache->xs[x] = ((f64)x / (f64)platformFbWidth) * (f64)gameFbWidth;
	}
}

// Sample the game framebuffer pixels to match our platform buffer
void x_FramebufferSampleFromGame(Render_Buffer *GameBuffer, framebuffer *PlatformBuffer, samplecache *Cache)
{
	u32 x;
	u32 y;
	samplecache_t px;
	u32 sample;
	for(y = 0; y < PlatformBuffer->subheight; y++)
	{
		const samplecache_t py = Cache->ys[y];
		const u32 offYBuffer = y * PlatformBuffer->subwidth;
		const u32 offYGame = py * GameBuffer->width;
		for(x = 0; x < PlatformBuffer->subwidth; x++)
		{
			px = Cache->xs[x];
			sample = GameBuffer->pixels[offYGame + px];
			PlatformBuffer->data[offYBuffer + x] = sample;
		}
	}
}

u64 x_MonitorRefreshRateToNs(u64 rateInHz)
{
	return (1.0 / (f64)rateInHz) * PLATFORM_S_TO_NS_CONVERSION_U64;
}

static void x_WindowTitleSet(Display *Dpy, Window Win, const char *title)
{
	XChangeProperty(Dpy, Win, XA_WM_NAME, XA_STRING, 8, PropModeReplace, (unsigned char *)title, platform_Strlen255(title));
}

b32 x_KeysymIsDown(const keys *Keys, Display *Dpy, const u32 Code)
{
	return Keys->Key[XKeysymToKeycode(Dpy, Code)].isDown;
}

b32 x_KeysymIsHeld(const keys *Keys, Display *Dpy, const u32 Code)
{
	return Keys->Key[XKeysymToKeycode(Dpy, Code)].isHeld;

}

b32 x_KeysymIsUp(const keys *Keys, Display *Dpy, const u32 Code)
{
	b32 a;
	b32 b;
	a = x_KeysymIsDown(Keys, Dpy, Code);
	b = x_KeysymIsHeld(Keys, Dpy, Code);
	return (!a && !b);
}

void x_KeysymSetToHeld(keys *Keys, Display *Dpy, const u32 Code)
{
	Keys->Key[XKeysymToKeycode(Dpy, Code)].isDown = false;
	Keys->Key[XKeysymToKeycode(Dpy, Code)].isHeld = true;
}

void x_ProcessGameButtonAsSinglePress(keys *Keys, input *Input, Display *Dpy,  u32 PlatformButton, game_button GameButton)
{
	if(x_KeysymIsDown(Keys, Dpy, PlatformButton))
	{
		x_KeysymSetToHeld(Keys, Dpy, PlatformButton);
		Input->buttons[GameButton].changed = true;
		Input->buttons[GameButton].is_down = true;

	}
	else if(x_KeysymIsHeld(Keys, Dpy, PlatformButton) || x_KeysymIsUp(Keys, Dpy, PlatformButton))
	{
		Input->buttons[GameButton].changed = true;
		Input->buttons[GameButton].is_down = false;
	}
}

void x_ProcessGameButtonAsHeld(keys *Keys, input *Input, Display *Dpy, u32 PlatformButton, game_button GameButton)
{
	if(x_KeysymIsDown(Keys, Dpy, PlatformButton) || x_KeysymIsHeld(Keys, Dpy, PlatformButton))
	{
		x_KeysymSetToHeld(Keys, Dpy, PlatformButton);
		Input->buttons[GameButton].changed = true;
		Input->buttons[GameButton].is_down = true;
	}
	else if(x_KeysymIsUp(Keys, Dpy, PlatformButton))
	{
		Input->buttons[GameButton].changed = true;
		Input->buttons[GameButton].is_down = false;
	}
}

void x_ProcessKeysm(keys *Keys, input *Input, Display *Dpy)
{
	// Platform controls
	// Pause
	if(x_KeysymIsDown(Keys, Dpy, XK_p))
	{
		x_IsPaused = !x_IsPaused;
		x_KeysymSetToHeld(Keys, Dpy, XK_p);
	}

	// Quit
	if(x_KeysymIsDown(Keys, Dpy, XK_q) || x_KeysymIsHeld(Keys, Dpy, XK_q))
	{
		x_TimeToQuit = 1;
		x_KeysymSetToHeld(Keys, Dpy, XK_q);
	}

	// Clear the platform profiler
	if(x_KeysymIsDown(Keys, Dpy, XK_r))
	{
		platform_TimedBlockClearAll();
		x_KeysymSetToHeld(Keys, Dpy, XK_r);
	}

	// Game controls

	// These are controls that only occur once per keypress
	x_ProcessGameButtonAsSinglePress(Keys, Input, Dpy, XK_l, BUTTON_CFG);
	x_ProcessGameButtonAsSinglePress(Keys, Input, Dpy, XK_Escape, BUTTON_ESC);
	x_ProcessGameButtonAsSinglePress(Keys, Input, Dpy, XK_Right, BUTTON_RIGHT);
	x_ProcessGameButtonAsSinglePress(Keys, Input, Dpy, XK_Left, BUTTON_LEFT);

	// These are controls occur every frame while the key is held
	x_ProcessGameButtonAsHeld(Keys, Input, Dpy, XK_Down, BUTTON_DOWN);
	x_ProcessGameButtonAsHeld(Keys, Input, Dpy, XK_Up, BUTTON_UP);
}

#define NET_WM_STATE_TOGGLE 2
// Thanks to user 'n. 1.8e9-where's-my-share m.' for this bit of wisdom
// https://stackoverflow.com/questions/10897503/opening-a-fullscreen-opengl-window

// Also CapelliC for this code snippet from freeglut on how they handle toggling for X11 windows
// https://stackoverflow.com/a/9065900
// Original link: https://stackoverflow.com/questions/9065669/x11-glx-fullscreen-mode
void x_WindowToggleFullscreen(Display *Dpy, Window Win)
{
	Atom WmState = XInternAtom(Dpy, "_NET_WM_STATE", False);
	Atom Fullscreen = XInternAtom(Dpy, "_NET_WM_STATE_FULLSCREEN", False);

    XEvent Event = {0};
    Event.type = ClientMessage;
    Event.xclient.window = Win;
    Event.xclient.message_type = WmState;
    Event.xclient.format = 32;
    Event.xclient.data.l[0] = NET_WM_STATE_TOGGLE; // Action
    Event.xclient.data.l[1] = Fullscreen;          // Atom
    Event.xclient.data.l[2] = 0;

    XSendEvent(Dpy, DefaultRootWindow(Dpy), False,
			SubstructureRedirectMask | SubstructureNotifyMask, &Event);
    XFlush(Dpy);
}



static b32 x_AtomIsPresent(Display *Dpy, const char *atomName)
{
	Atom A;
	A = XInternAtom(Dpy, atomName, True);
	return (A != None);
}

static b32 x_AtomContains(Display *Dpy, unsigned long *pAtom, long unsigned int nItems, const char *subatomName)
{
	b32 result = false;
	const char *atomName;
	unsigned long int i;
	int streq;
	if(nItems && pAtom)
	{
		for(i = 0; i < nItems; i++)
		{
			atomName = XGetAtomName(Dpy, pAtom[i]);
			streq = strncmp(atomName, subatomName, 1024);
			if(streq == 0)
			{
				result = true;
				break;
			}
		}
	}
	return result;

}
// @Hack: This is a bit of a hack since we only care about 1 subatom at the moment, but it could be expanded to take a char** if necessary
static b32 x_WindowAtomIsPresent(Display *Dpy, Window Win, const char *atomName, const char *subatomName)
{
	b32 result;
	Atom A;
	Atom AtomRet;
	int fmtRet;
	long unsigned int itemsRet;
	unsigned long bytesRet;
	unsigned char *propRet = NULL;
	unsigned long *pAtom;

	A = XInternAtom(Dpy, atomName, True);
	// Note: The atom may not appear right away, as there is latency between the command and the X server.
	XGetWindowProperty(Dpy, Win, A, 0, sizeof A, False, XA_ATOM, &AtomRet, &fmtRet, &itemsRet, &bytesRet, &propRet);
	result = (propRet != NULL);

	if(subatomName)
	{
		if(result && (itemsRet > 0))
		{
			b32 subresult;
			pAtom = (unsigned long *)propRet;
			subresult = x_AtomContains(Dpy, pAtom, itemsRet, subatomName);
			result = subresult;
			if(!subresult)
			{
				log("Found atom '%s', but could not find atom '%s'\n", atomName, subatomName);
			}
		}
	}
	return result;
}

// platform_* functions
//--------------------------------------------------------------------------------
// Returns the current time in nanoseconds
u64 platform_GetTimeNs()
{
	u64 result = 0;
	struct timespec Ts = {0};
	clock_gettime(CLOCK_MONOTONIC, &Ts);
	result = ((u64)Ts.tv_sec * PLATFORM_S_TO_NS_CONVERSION_U64) + (u64)Ts.tv_nsec;
	return result;
}

void platform_SleepNs(u64 ns)
{
	struct timespec Ts;
	Ts.tv_sec = ns / PLATFORM_S_TO_NS_CONVERSION_U64;
	Ts.tv_nsec = ns - (Ts.tv_sec * PLATFORM_S_TO_NS_CONVERSION_U64);
	clock_nanosleep(CLOCK_MONOTONIC, 0, &Ts, NULL);
}

// Not a true hardware-level vsync of course, more like a frame limiter
void platform_Vsync(u64 nsElapsed, u64 nsTarget)
{
	if(nsElapsed < nsTarget)
	{
		u64 nsSleepFor;
		nsSleepFor = nsTarget - nsElapsed;
		platform_SleepNs(nsSleepFor);
	}
}

u8 platform_Strlen255(const char *s)
{
	u8 count = 0;
	while(s[count] && count < 255)
	{
		count++;
	}
	return count;
}

// Wrapper for mmap
// The programmer is required to keep track of the allocation size for munmap
void *platform_MemoryMap(u64 bytes)
{
	void *memory = NULL;
	memory = mmap(NULL, bytes, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	Assert(memory != NULL || memory != MAP_FAILED);
	return memory;
}

// Wrapper for munmap
void platform_MemoryUnmap(void *ptr, u64 bytes)
{
	munmap(ptr, bytes);
}
