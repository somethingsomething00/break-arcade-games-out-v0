#if PLATFORM_PROFILER
#ifndef _LINUX_PLATFORM_PROFILER_C_
#define _LINUX_PLATFORM_PROFILER_C_

double platform_GetTimerFrequency()
{
	return 1E9;
}

u64 platform_GetTime()
{
	u64 result;
	struct timespec Ts;
	clock_gettime(CLOCK_MONOTONIC, &Ts);
	return (Ts.tv_sec * (u64)1E9) + Ts.tv_nsec;
}


// Note: We don't want to include a heavy intrinsic library like x86intrin for a simple rdtsc call.
// Therefore it is inlined. It's probably less portable this way though.
u64 GetCycles()
{
	// rdtsc stores its result in rax and rdx respectively
	// rax contains the low 32 bits (as a 32 bit value)
	// rdx contains the high 32 bits (as a 32 bit value)
	// Note: GNU assembler syntax
	uint64_t result = 0;
	asm volatile(
	"rdtsc\n"
	"shl $32, %%rdx\n"
	"or %%rdx, %0\n" // In gnu assembler, looks like the second register stores the result
	:"=a" (result)
	:
	:"rdx");
	return result;

}

#endif /* _LINUX_PLATFORM_PROFILER_C_ */

#endif /* PLATFORM_PROFILER */
