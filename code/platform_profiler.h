#ifndef _PLATFORM_PROFILER_H_
#define _PLATFORM_PROFILER_H_

// Common definitions for the platform profiler

typedef enum
{
	TB_GAME,
	TB_IMAGE,
	TB_MEMCPY,
	TB_TEXTURE,
	TB_LOOP,
	TB_BEFORE_GAME,
	TB_CLEAR_SCREEN,
	TB_TOTAL,
} timed_block_t;


typedef enum
{
	CB_MEMCPY,
	CB_TOTAL,
} cycle_block_t;



#if PLATFORM_PROFILER

typedef struct
{
	u64 begin;
	u64 end;
	double seconds;
	u64 hits;
} timed_block;

typedef struct
{
	u64 cycles;
	u64 hits;
} cycle_block;


u64 GetCycles();
/* Not meant to be used by the platform layer explicitly. 
   Returns processor clock cycles */

void platform_TimedBlockBegin(timed_block_t id);
void platform_TimedBlockEnd(timed_block_t id);
/* Capture a timestamp with a high precision timer.
   Each capture increments the `hits` field in timed_block */

void platform_TimedBlockList();
/* List the average duration of each block. 
   Displays (milliseconds / hits) as the result.  */

void platform_TimedBlockClearAll();
/* Clears accumulated hits for each timed_block id. */

const char *HumanReadableTimedBlock(timed_block_t id);
/* Not meant to be used by the platform layer explicitly.
   Returns a stringified version of the id that's passed in */


void platform_CycleBlockBegin(cycle_block_t id);
void platform_CycleBlockEnd(cycle_block_t id);
/* Capture processor cycles with rdtsc (x86 only).
   Each capture increments the `hits` field in cycle_block */

void platform_CycleBlockEndHits(cycle_block_t id, u64 hits);
/* Capture processor cycles with rdtsc (x86 only).
   Provide a known quanity of captures to get the average (cycles / hit) */
	
void platform_CycleBlockList();
/* List the average cycle count of each block. 
   Displays (cycles / hits) as the result.  */

const char *HumanReadableCycleBlock(cycle_block_t id);
/* Not meant to be used by the platform layer explicitly.
   Returns a stringified version of the id that's passed in */

	#ifndef _WIN_32
	#include "./linux_platform_profiler.h"
	#else
	#include "./windows_platform_profiler.h"
	#endif /* _WIN32 */
#else

#define platform_TimedBlockBegin(...)
#define platform_TimedBlockEnd(...)
#define platform_TimedBlockList(...)
#define platform_TimedBlockClearAll(...)
#define HumanReadableTimedBlock(...)

#define platform_CycleBlockBegin(...)
#define platform_CycleBlockEnd(...)
#define platform_CycleBlockEndHits(...)
#define platform_CycleBlockList(...)
#define HumanReadableCycleBlock(...)

#endif /* PLATFORM_PROFILER */

#endif /* _PLATFORM_PROFILER_H_ */
