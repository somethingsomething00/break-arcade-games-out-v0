#ifndef _WINDOWS_PLATFORM_PROFILER_C_
#define _WINDOWS_PLATFORM_PROFILER_C_

static LARGE_INTEGER platform_GlobalPerformanceFrequency = {0};

double platform_GetTimerFrequency()
{
	// Note: We cache the result of QueryPerformanceFrequency
	// This avoids a divide by zero bug, however writing the function this way probably prevents the compiler from inlining this call
	static int isInitialized = 0;
	if(!isInitialized)
	{
		platform_GlobalPerformanceFrequency = QueryPerformanceFrequency();
		isInitialized = 1;
	}
	return platform_GlobalPerformanceFrequency;
}

u64 platform_GetTime()
{
	LARGE_INTEGER result;
	QueryPerformanceCounter(&result);
	return result.QuadPart;
}

// Note: The asm block is untested, but it should work
// For now we include intrin.h in windows_platform_profiler.h to access rdtsc
u64 GetCycles()
{
	// rdtsc stores its result in rax and rdx respectively
	// rax contains the low 32 bits (as a 32 bit value)
	// rdx contains the high 32 bits (as a 32 bit value)

#if 0
	u64 result;
	__asm
	{
		rdtsc
		shr rdx, 32
		or rax, rdx
		mov result, rax
	}
	return result;
#endif
	return __rdtsc();
}

#endif /* _WINDOWS_PLATFORM_PROFILER_C_ */
