

#define HUGE_NUMBER 100000000

global_variable b32 running = true;

#define KiB(a) (1024LL*(a))
#define MiB(a) (1024LL*(KiB(a)))
#define GiB(a) (1024LL*(MiB(a)))
#define TiB(a) (1024LL*(GiB(a)))

#define array_count(a) (sizeof(a) / sizeof((a)[0]))

#if DEVELOPMENT
#include "assert.h"
//#define assert(c) /*{if (!(c)) {*(int*)0=0;}else{}}*/
#else
#define assert(c)
#endif

#define invalid_default_case default: {assert(0);}
#define invalid_code_path assert(0);

void
zero_size(void* mem, u64 size) {
    u8* dest = (u8*)mem;
    for (u64 i = 0; i < size; i++) {
        *dest++ = 0;
    }
}

#define zero_array(a) zero_size(a, sizeof(a))
#define zero_struct(s) zero_size(&(s), sizeof(s))
