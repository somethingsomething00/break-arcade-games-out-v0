#ifndef _X_SHM_H_
#define _X_SHM_H_
#include <sys/ipc.h>
#include <sys/shm.h>
#include <X11/extensions/XShm.h>

typedef struct
{
	framebuffer Fb;
	XImage *Image;
	XShmSegmentInfo SegmentInfo;
} singlebuffer;

typedef struct
{
	singlebuffer Front;
	singlebuffer Back;
} doublebuffer;


static void x_DoublebufferCreate(doublebuffer *DbReturn, Display *Dpy, XWindowAttributes *WindowAttributes, u32 width, u32 height);
static void x_SwapBuffers(doublebuffer *Db);
static void x_DoublebufferCleanup(doublebuffer *Db, Display *Dpy);
static void x_DoublebufferResetOnResize(doublebuffer *Db);

#endif /* _X_SHM_H_ */
