#!/bin/sh

# Note to self: xcompmgr causes stuttering in the gl version, it's not the code itself

set_common_variables()
{
	cc="clang"
	lib="-lm"
	bin="break-arcade-games-out"
	cflags=""
	optimize_flags=""
	optimize_flags+=" -O2"
	optimize_flags+=" -ffast-math"
	optimize_flags+=" -march=native"
	optimize_flags+=" -funroll-loops"
	debug_flags=" -g3"
	warn="-Wall -Wextra -Wpedantic"
}

set_common_variables

####################################
# OpenGL
####################################
gl_set_default()
{
	src="linux_gl_platform.c"
	lib+=" -lGL -lGLEW -lglfw"
	bin+="-gl"
}

gl_set_release()
{
	gl_set_default
	cflags+="$optimize_flags"
	bin+="-release"
}

gl_set_default_debug()
{
	gl_set_default
	cflags+="$debug_flags"
	bin+="-debug"
}

gl_set_release_debug()
{
	gl_set_release
	cflags+="$debug_flags"
	bin+="-debug"
}


####################################
# X11
####################################
x11_set_default()
{
	src="linux_x11_platform.c"
	lib+=" -lX11 -lXext -lXrandr"
	bin+="-x11"
}

x11_set_default_debug()
{
	x11_set_default
	cflags+="$debug_flags"
	bin+="-debug"
}

x11_set_release()
{
	x11_set_default
	bin+="-release"
	cflags+="$optimize_flags"
}

x11_set_release_debug()
{
	x11_set_release
	cflags+="$debug_flags"
	bin+="-debug"
}


####################################
# SDL
####################################
sdl_set_default()
{
	src="linux_sdl_platform.c"
	bin+="-sdl"
	lib+=" -lSDL2"
}

sdl_set_default_debug()
{
	sdl_set_default
	cflags+="$debug_flags"
	bin+="-debug"
}

sdl_set_release()
{
	sdl_set_default
	cflags+="$optimize_flags"
	bin+="-release"
}

sdl_set_release_debug()
{
	sdl_set_release
	cflags+="$debug_flags"
	bin+="-debug"
}

remove_targets()
{
	rm -fv break-arcade-games-out-gl
	rm -fv break-arcade-games-out-gl-debug
	rm -fv break-arcade-games-out-gl-release
	rm -fv break-arcade-games-out-gl-release-debug

	rm -fv break-arcade-games-out-x11
	rm -fv break-arcade-games-out-x11-debug
	rm -fv break-arcade-games-out-x11-release
	rm -fv break-arcade-games-out-x11-release-debug

	rm -fv break-arcade-games-out-sdl
	rm -fv break-arcade-games-out-sdl-debug
	rm -fv break-arcade-games-out-sdl-release
	rm -fv break-arcade-games-out-sdl-release-debug
}


# All of our variables have been set
# We can call this function and compile the game!
compile_command_set_and_compile()
{
	additional_args="$1"
	compile_command="$cc $src $lib $cflags -o $bin $additional_args"

	printf "\033[1;32mCompiling target: $bin\033[0m\n"
	# printf "Compiling target: $bin\n"
	echo $compile_command
	$compile_command && printf "\033[1;32mBuilt target: $bin\033[0m\n"
	# $compile_command && printf "$bin\n"
}


build_all_release_targets()
{
	com_args="$1"
	set_common_variables
	sdl_set_release
	compile_command_set_and_compile "$com_args" &
	
	set_common_variables
	gl_set_release
	compile_command_set_and_compile "$com_args" &
	
	set_common_variables
	x11_set_release
	compile_command_set_and_compile "$com_args" &
	wait
}

build_help()
{
	printf "Usage: $0 COMMAND [COMPILER-FLAGS]...\n"
	printf "Supported commands:\n"
	printf "gl                    Use OpenGL as the rendering backend. Compiler optimizations disabled.\n"
	printf "gl-debug              Use OpenGL as the rendering backend. Compiler optimizations disabled. Debug information included.\n"
	printf "gl-release            Use OpenGL as the rendering backend. Compiler optimizations enabled.\n"
	printf "gl-release-debug      Use OpenGL as the rendering backend. Compiler optimizations enabled. Debug information included.\n"
	
	printf "\n"
	
	printf "x11                   Use X11 as the rendering backend. Compiler optimizations disabled.\n"
	printf "x11-debug             Use X11 as the rendering backend. Compiler optimizations disabled. Debug information included.\n"
	printf "x11-release           Use X11 as the rendering backend. Compiler optimizations enabled.\n"
	printf "x11-release-debug     Use X11 as the rendering backend. Compiler optimizations enabled. Debug information included.\n"
	
	printf "\n"

	printf "sdl                   Use SDL as the rendering backend. Compiler optimizations disabled.\n"
	printf "sdl-debug             Use SDL as the rendering backend. Compiler optimizations disabled. Debug information included.\n"
	printf "sdl-release           Use SDL as the rendering backend. Compiler optimizations enabled.\n"
	printf "sdl-release-debug     Use SDL as the rendering backend. Compiler optimizations enabled. Debug information included.\n"
	
	printf "\n"
	
	printf "all                   Build all release targets.\n"
	printf "clean                 Remove all build targets.\n"
	
	printf "%s\n-h                    Show this help.\n"

	printf "\nYou may also supply any other compiler flags to the script provided that the first argument is one of the valid targets above.\n"
}

arg=$1
compiler_args=${@:2:$#}
case $arg in
	"gl")
		gl_set_default
		;;
	"gl-release")
		gl_set_release
		;;
	"gl-debug")
		gl_set_default_debug
		;;
	"gl-release-debug")
		gl_set_release_debug
		;;
	"x11")
		x11_set_default
		;;
	"x11-debug")
		x11_set_default_debug
		;;
	"x11-release")
		x11_set_release
		;;
	"x11-release-debug")
		x11_set_release_debug
		;;
	"sdl")
		sdl_set_default
		;;
	"sdl-release")
		sdl_set_release
		;;
	"sdl-debug")
		sdl_set_default_debug
		;;
	"sdl-release-debug")
		sdl_set_release_debug
		;;
	"clean")
		remove_targets
		exit
		;;
	"all")
		build_all_release_targets "$compiler_args"
		exit
		;;
	"-h")
		build_help
		exit
		;;
	"") # Default target
		x11_set_release
		;;
	*)
		echo -e "\033[31mError: \033[33m'$arg'\033[0m is not a valid build command"
		build_help
		exit
		;;
esac

compile_command_set_and_compile "$compiler_args"
