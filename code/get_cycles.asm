bits 64
section .text
;Nasm compatible

;'Nix
;Build with `nasm -felf64 GetCycles.asm` and include the object file as input to the compiler

;Windows (untested)
;Build with `nasm -fwin64 GetCycles.asm` and include the object file as input to the compiler


;rdtsc stores its result in rax and rdx respectively
;rax contains the low 32 bits (as a 32 bit value)
;rdx contains the high 32 bits (as a 32 bit value)
global GetCyclesAsm
GetCyclesAsm:
	rdtsc
	shl rdx, 32
	or rax, rdx
	ret
