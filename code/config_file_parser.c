#include "config_defaults.h"
// @Hack! We should just set a boolean flag or something
typedef void(add_screenshake_fp)(f32 factor);
extern add_screenshake_fp *add_screenshake;
extern void add_screenshake_impl(f32 factor);
extern void add_screenshake_dummy(f32 factor);

internal void
eat_whitespaces(String *s) {
    for (s64 i = 0; i < s->size;) {
        if (*s->data != ' ' && *s->data != '\r' && *s->data != '\n' && *s->data != '\t') return;
        s->data++;
        s->size--;
    }
}

internal String
consume_next_word(String *s) {
    eat_whitespaces(s);
    String result = *s;
    result.size = 0;
    for (s64 i = 0; i < s->size;) {
        if (*s->data == ' ' || *s->data == '\r' || *s->data == '\n' || *s->data == '\t') {
            eat_whitespaces(s);
            return result;
        }
        s->data++;
        s->size--;
        result.size++;
    }
    
    eat_whitespaces(s);
    return result;
}

internal f32
parse_float_value(String *s) {
    
    consume_next_word(s);
    
    String float_string = consume_next_word(s);
    f32 result = 0.f;
    
    int decimal = 0;
    for (s64 i = 0; i < float_string.size; i++) {
        if (float_string.data[i] >= '0' && float_string.data[i] <= '9') {
            int digit = float_string.data[i] - '0';
            if (decimal == 0) {
                result *= 10.f;
                result += (f32)digit;
            } else {
                result += (f32)digit / (f32)decimal;
                decimal *= 10;
            }
            
        } else if (float_string.data[i] == '.' || float_string.data[i] == ',') {
            decimal = 10;;
        } else return 1.f;
    }
    
    return result;
}

internal b32
parse_b32_value(String *s) {
    consume_next_word(s);
    String val = consume_next_word(s);
    
    String true_string = {"true", 4};
    if (strings_match(val, true_string)) {
        return true;
    }
    
    return false;
}


enum
{
	MASK_SCREENSHAKE = 0x1,
	MASK_MOUSE_SENSITIVITY = 0x2,
};

#define HAVE_SCREENSHAKE(a_) ((a_) & MASK_SCREENSHAKE)
#define HAVE_MOUSE_SENSITIVITY(a_) ((a_) & MASK_MOUSE_SENSITIVITY)

internal void
load_config() {
    String file = os_read_config_file();

	// @Bug / @Workaround
	// consume_next_word and eat_whitespaces modified the actual data pointer address contained in the String struct
	// This was causing a free bug since the base pointer was no longer what the allocator was expecting
	// This is a workaround instead of a bug fix, but we just save the base pointer before the struct is passed into the string processing functions
	
	void *pData;
	pData = file.data;
    String mouse_sensitivity_string = {"mouse_sensitivity", 17};
    String windowed_string = {"windowed", 8};
    String lock_fps_string = {"lock_fps", 8};
    String screen_shake_string = {"screen_shake", 12};
	u32 configOption = 0;
    
    while (file.size) {
        String keyword = consume_next_word(&file);
        if (strings_match(keyword, mouse_sensitivity_string)) {
			configOption |= MASK_MOUSE_SENSITIVITY;
            f32 value = parse_float_value(&file);
			// @Bugfix: Adjust default mouse sensitivity value on failure to parse its value for the key 'mouse_sensitivity' in the config file
			// Previous behaviour: If the key 'mouse_sensitivity' was present but its value was unable to be parsed, it would default to the minimum clamp value. 
			// New behaviour: On failure to parse a valid floating point number, the sensitivity is reset back to the default as defined in config_defaults.h
			if(value < mouse_sensitivity_min) value = mouse_sensitivity_default;
            mouse_sensitivity = clampf(mouse_sensitivity_min, value, mouse_sensitivity_max);
        } else if (strings_match(keyword, windowed_string)) {
            b32 value = parse_b32_value(&file);
            if (value) os_toggle_fullscreen();
        } 
		// @New: We now parse for the screen_shake string
		else if (strings_match(keyword, screen_shake_string)) {
            b32 value = parse_b32_value(&file);
			configOption |= MASK_SCREENSHAKE;
    		if (value) add_screenshake = add_screenshake_impl;
			else add_screenshake = add_screenshake_dummy;
        } 
		else if (strings_match(keyword, lock_fps_string)) {
            lock_fps = parse_b32_value(&file);
        }
    }

	// @New: Set some default values in case an option was removed during runtime
	// Previous behaviour: If a config value was completely removed during runtime, the old value would persist
	// New behaviour: If a config value is removed during runtime (i.e the option was completely missing), we set these default values
	if(!(HAVE_SCREENSHAKE(configOption))) {
		add_screenshake = add_screenshake_default;
	}
	if(!(HAVE_MOUSE_SENSITIVITY(configOption))) {
		mouse_sensitivity = mouse_sensitivity_default;
	}
	file.data = pData;
	os_free_file(file);
}
